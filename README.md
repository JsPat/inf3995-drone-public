# drone

The embedded software used in each drone of the cluster


### To run on drone:
Install crazyflie-firmware, crazyflie-client-python and crazyflie-lib-python (should already be there, but if it's not the case, they can be found here:
https://github.com/bitcraze/crazyflie-firmware/tree/1a4cc9acf3edefb3833b72e05095be9c8b0fc887
https://github.com/bitcraze/crazyflie-clients-python
https://github.com/bitcraze/crazyflie-lib-python
).


Go to drone/ and run these commands:
```bash
git submodule init
git submodule update
```
Then, go in drone/tools/make. Copy 'config.mk.examples' and paste it besides.
Rename the copy 'config.mk' and add these lines at the beginning:
```bash
PLATFORM=CF2
DEBUG=1
```


Build the firmware (run these commands in drone/):
```bash
make
```

Install the python libraries (run these commands in drone/):
```bash
sudo groupadd plugdev
sudo usermod -a -G plugdev $USER
sudo nano /etc/udev/rules.d/99-crazyradio.rules
```
In the nano editor, add these lines (to save, afterwards: Ctrl-o, Enter, Ctrl-x):
```bash
# Crazyradio (normal operation)
SUBSYSTEM=="usb", ATTRS{idVendor}=="1915", ATTRS{idProduct}=="7777", MODE="0664", GROUP="plugdev"
# Bootloader
SUBSYSTEM=="usb", ATTRS{idVendor}=="1915", ATTRS{idProduct}=="0101", MODE="0664", GROUP="plugdev"
```
Then, open an other file with the nano editor (to save, afterwards: Ctrl-o, Enter, Ctrl-x):
```bash
sudo nano /etc/udev/rules.d/99-crazyflie.rules
```
And add this line:
```bash
SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="5740", MODE="0664", GROUP="plugdev"
```

Then run these commands:
```bash
sudo udevadm control --reload-rules
sudo udevadm trigger
```

Build the python client (run these commands in drone/):
\
(if it doesn't work, you can clone the original repository, do these commands, then remove the .git and .github folders (or add them to submodules) by follofinw the steps here: https://github.com/bitcraze/crazyflie-clients-python)
```bash
python3 -m pip install cfclient
python3 -m pip install -e .
cfclient
```

Then, plug the radio in your PC, and press the ON/OFF button on the drone one time fast, then one other time during 3 seconds.

Then, go to drone/app_api and run these commands:
```bash
make clean
make
make cload
```

The lights on the drone should change and it should make some noise if the flash was succesful.
You can also go on the python client (the window that appeared after "cfclient") and connect the drone.


# Start drone tests

In drone/ enter this command:
```bash
docker run --rm -it bitcraze/toolbelt
```
Then, you should see instructions telling you to add a line to your .bashrc, so enter these commands:
```bash
cd ~
nano ./bashrc
```
Then add your line in ./bashrc (this is mine for example), save and close the editor:
```bash
alias tb='docker run --rm -it -e "HOST_CW_DIR=${PWD}" -e "CALLING_HOST_NAME=$(hostname)" -e "CALLING_UID"=$UID -e "CALLING_OS"=$(uname) -v ${PWD}:/tb-module -v ${HOME}/.ssh:/root/.ssh -v /var/run/docker.sock:/var/run/docker.sock bitcraze/toolbelt'
```
To run one specific test, run this command in drone/ (where TEST_FILE is the name of your file):
```bash
tb make unit FILES=app_api/test/ledSystem/TEST_FILE
```
For example, to run the ledController.c tests, enter this command:
```bash
tb make unit FILES=app_api/test/ledSystem/test_ledController.c
```

To run all the tests files of app_api, enter these commands in drone/:
```bash
chmod + x ./all_app_api_tests.sh
./all_app_api_tests.sh
```
<br >
<br >

# To run the simulation through ARGoS
You first have to install ARGoS docker with these comamnds:
```bash
git clone https://github.com/lajoiepy/argos3_docker_example.git
cd argos3_docker_example
docker build . --tag argos-example --network host --build-arg CODE_UPDATE=1
```

Then, you have to install X11Docker with this command:
```bash
curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | sudo bash -s -- --update
```

You now have to go on the root of Mainrepo. Normally, if you have cloned this drone repository from the Mainrepo recursively, you should be able to run ARGoS simulation with these commands:
```bash
cd ..
./simulation.sh
```

# Start ARGoS tests
When ARGoS is already set on your system, you can go to Mainrepo (that should be a level before as said previously) and run the simulation tests by running these commands:
```bash
cd ..
./simulation.sh test
```


<br >
<br >
<br >

# Crazyflie Firmware  [![CI](https://github.com/bitcraze/crazyflie-firmware/workflows/CI/badge.svg)](https://github.com/bitcraze/crazyflie-firmware/actions?query=workflow%3ACI)

This project contains the source code for the firmware used in the Crazyflie range of platforms, including
the Crazyflie 2.X and the Roadrunner.

### Crazyflie 1.0 support

The 2017.06 release was the last release with Crazyflie 1.0 support. If you want
to play with the Crazyflie 1.0 and modify the code, please clone this repo and
branch off from the 2017.06 tag.

## Dependencies

You'll need to use either the [Crazyflie VM](https://wiki.bitcraze.io/projects:virtualmachine:index),
[the toolbelt](https://wiki.bitcraze.io/projects:dockerbuilderimage:index) or
install some ARM toolchain.

### Install a toolchain

#### OS X
```bash
brew tap PX4/homebrew-px4
brew install gcc-arm-none-eabi
```

#### Debian/Ubuntu

Tested on Ubuntu 14.04 64b/16.04 64b/18.04 64b/20.04 64b/20.10 64b:

For Ubuntu 14.04 :

```bash
sudo add-apt-repository ppa:terry.guo/gcc-arm-embedded
sudo apt-get update
sudo apt-get install libnewlib-arm-none-eabi
```

For Ubuntu 16.04 and 18.04:

```bash
sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
sudo apt-get update
sudo apt install gcc-arm-embedded
```

For Ubuntu 20.04 and 20.10:

```bash
sudo apt-get install make gcc-arm-none-eabi
```

#### Arch Linux

```bash
sudo pacman -S community/arm-none-eabi-gcc community/arm-none-eabi-gdb community/arm-none-eabi-newlib
```

#### Windows

The GCC ARM Embedded toolchain for Windows is available at [launchpad.net](https://launchpad.net/gcc-arm-embedded/+download). Download the zip archive rather than the executable installer. There are a few different systems for running UNIX-style shells and build systems on Windows; the instructions below are for [Cygwin](https://www.cygwin.com/).

Install Cygwin with [setup-x86_64.exe](https://www.cygwin.com/setup-x86_64.exe). Use the standard `C:\cygwin64` installation directory and install at least the `make` and `git` packages.

Download the latest `gcc-arm-none-eabi-*-win32.zip` archive from [launchpad.net](https://launchpad.net/gcc-arm-embedded/+download). Create the directory `C:\cygwin64\opt\gcc-arm-none-eabi` and extract the contents of the zip file to it.

Launch a Cygwin terminal and run the following to append to your `~/.bashrc` file:
```bash
echo '[[ $PATH == */opt/gcc-arm-none-eabi/bin* ]] || export PATH=/opt/gcc-arm-none-eabi/bin:$PATH' >>~/.bashrc
source ~/.bashrc
```

Verify the toolchain installation with `arm-none-eabi-gcc --version`

### Cloning

This repository uses git submodules. Clone with the `--recursive` flag

```bash
git clone --recursive https://github.com/bitcraze/crazyflie-firmware.git
```

If you already have cloned the repo without the `--recursive` option, you need to
get the submodules manually

```bash
cd crazyflie-firmware
git submodule init
git submodule update
```


## Compiling

### Crazyflie 2.X

This is the default build so just running ```make``` is enough or:
```bash
make PLATFORM=cf2
```

or with the toolbelt

```bash
tb make PLATFORM=cf2
```

### Roadrunner

Use the ```tag``` platform

```bash
make PLATFORM=tag
```

or with the toolbelt

```bash
tb make PLATFORM=tag
```


### config.mk
To create custom build options create a file called `config.mk` in the `tools/make/`
folder and fill it with options. E.g.
```
PLATFORM=CF2
DEBUG=1
```
More information can be found on the
[Bitcraze documentation](https://www.bitcraze.io/documentation/repository/crazyflie-firmware/master/)


# Make targets:

```
all        : Shortcut for build
compile    : Compile cflie.hex. WARNING: Do NOT update version.c
build      : Update version.c and compile cflie.elf/hex
clean_o    : Clean only the Objects files, keep the executables (ie .elf, .hex)
clean      : Clean every compiled files
mrproper   : Clean every compiled files and the classical editors backup files

cload      : If the crazyflie-clients-python is placed on the same directory level and
             the Crazyradio/Crazyradio PA is inserted it will try to flash the firmware
             using the wireless bootloader.
flash      : Flash .elf using OpenOCD
halt       : Halt the target using OpenOCD
reset      : Reset the target using OpenOCD
openocd    : Launch OpenOCD
```

# Unit testing

## Running all unit tests

With the environment set up locally

        make unit

with the docker builder image and the toolbelt

        tb make unit

## Running one unit test

When working with one specific file it is often convenient to run only one unit test

       make unit FILES=test/utils/src/test_num.c

or with the toolbelt

       tb make unit FILES=test/utils/src/test_num.c

## Running unit tests with specific build settings

Defines are managed by make and are passed on to the unit test code. Use the
normal ways of configuring make when running tests. For instance to run test
for Crazyflie 1

      make unit LPS_TDOA_ENABLE=1

## Dependencies

Frameworks for unit testing and mocking are pulled in as git submodules.

The testing framework uses ruby and rake to generate and run code.

To minimize the need for installations and configuration, use the docker builder
image (bitcraze/builder) that contains all tools needed. All scripts in the
tools/build directory are intended to be run in the image. The
[toolbelt](https://wiki.bitcraze.io/projects:dockerbuilderimage:index) makes it
easy to run the tool scripts.



