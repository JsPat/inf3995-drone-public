#!/bin/bash

# Color codes
RED="\e[0;31m"
GREEN="\e[0;32m"
RESET="\e[0m"

NUMBER_TESTS_FAILED=0

# Run all test files in app_api and print the result of each
for TEST_FILE in ./app_api/test/**/*; do
    echo $TEST_FILE
    RESULT=$(docker run --rm -e "HOST_CW_DIR=${PWD}" -e "CALLING_HOST_NAME=$(hostname)" \
        -e "CALLING_UID"=$UID -e "CALLING_OS"=$(uname) -v ${PWD}:/tb-module -v ${HOME}/.ssh:/root/.ssh \
        -v /var/run/docker.sock:/var/run/docker.sock bitcraze/toolbelt \
        make unit FILES="$TEST_FILE" | grep -A 1 '\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-' | tail -1)
    echo $RESULT
    if [[ $RESULT != *"0 Failures"* ]]; then
        NUMBER_TESTS_FAILED=$(($NUMBER_TESTS_FAILED+1))
    fi
done

# Show end result of all tests
echo -e
if [[ $NUMBER_TESTS_FAILED = 0 ]]; then
    echo -e "${GREEN}SUCCESS${RESET}"
else
    echo -e "${RED}FAIL${RESET}"
fi


