/**
 * Main file of the APP API
 * 
 * Inspired from the app_main.c from the Crazyflie firmware git.
 *      It can be found here:
 *      https://github.com/bitcraze/crazyflie-firmware/blob/master/app_api/src/app_main.c
 * 
 */


#include "appMain.h"
#define DEBUG_MODULE "APPMAIN"

static enum State state = STATE_STANBY;
uint8_t batteryLevel = 0;


void appMain() {
    vTaskDelay(M2T(STARTUP_DELAY));
    command = COMMAND_NO_COMMAND;
    initializePositionSensor();
    // Register the callback function to receive messages between drones
    p2pRegisterCB(p2pCallbackHandler);
    initializeCommunication();
    
    while(1) {
        vTaskDelay(M2T(STEP_DELAY));
        command = receiveMessage();
        dispatchCommand();

        if (state != STATE_CHARGING
        && state != STATE_UPDATING
        && state != STATE_CRASHED) {
            sendInfoMappingMessagePeriodic();
        }

        // Get battery level and return to base or land if too low
        batteryLevel = getBatteryLevel();
        if (batteryLevel <= BATTERY_VERY_LOW) {
            state = STATE_LANDING;
        } else if (batteryLevel <= BATTERY_LOW) {
            if (state == STATE_IN_MISSION) {
                state = STATE_RETURN_TO_BASE;
            } else {
                state = STATE_LANDING;
            }
        }

        switch (state)
        {
        case STATE_STANBY:
            break;
        case STATE_TAKEOFF:
            state = STATE_IN_MISSION;
            break;
        case STATE_IN_MISSION:
            executeMissionStep();
            break;
        case STATE_LANDING:
            if(land()){
                state = STATE_STANBY;
            }
            break;
        case STATE_NEED_RECHARGE:
            break;
        case STATE_CHARGING:
            break;
        case STATE_UPDATING:
            break;
        case STATE_CRASHED:
            break;
        case STATE_RETURN_TO_BASE:
            if(returnToBase()){
                state = STATE_LANDING;
            }
            break;
        default:
            break;
        }
    }
}

void dispatchCommand() {
    switch (command)
    {
    case COMMAND_STANDBY:
        state = STATE_STANBY;
        break;
    case COMMAND_LED:
        turnOnLed();
        break;
    case COMMAND_TAKEOFF:
        state = STATE_TAKEOFF;        
        break;
    case COMMAND_LAND:
        state = STATE_LANDING;
        break;
    case COMMAND_RETURN_TO_BASE:
        state = STATE_RETURN_TO_BASE;
        break;
    case COMMAND_UPDATE:
        break;
    default:
        break;
    }
}

void sendInfoMappingMessagePeriodic() {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = TICK_FREQUENCY_MESSAGE_TO_SEND;
    // Initialise the xLastWakeTime variable with the current time
    xLastWakeTime = xTaskGetTickCount();
    // Wait for the next cycle
    vTaskDelayUntil(&xLastWakeTime, xFrequency);
    sendInfoMappingMessage(state);
}

