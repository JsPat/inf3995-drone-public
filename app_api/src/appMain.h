/**
 * Main file of the APP API
 * 
 * Inspired from the app_main.c from the Crazyflie firmware git.
 *      It can be found here:
 *      https://github.com/bitcraze/crazyflie-firmware/blob/master/app_api/src/app_main.c
 * Also inspired from the app.c from the Crazyflie firmware git.
 *      It can be found here:
 *      https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/swarm_demo/app.c
 * 
 */


#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include <pthread.h> 

#include "common/coordinates.h"
#include "common/state.h"
#include "ledSystem/ledController.h"
#include "controlSystem/controlUnit.h"
#include "communicationSystem/messageService.h"

#define STARTUP_DELAY 1000
#define STEP_DELAY 10

/**
 * Main function of the APP API
 */
void appMain();

/**
 * To make the drone follow the received command by changing it's state
 */
void dispatchCommand();

/**
 * Sends info and mapping message to station periodically
 */
void sendInfoMappingMessagePeriodic();

