/**
 * All constants needed for the control of the drone
 */

#ifndef CONTROL_CONSTANTS_H
#define CONTROL_CONSTANTS_H


#define FRONTSIDE 0
#define LEFTSIDE 1
#define BACKSIDE 2
#define RIGHTSIDE 3
#define DOWNSIDE 4
#define STOP 5

#define MAX_SENSORS 4
#define NO_SIDE_AVAILABLE 0
#define ONE_SIDE_AVAILABLE 1
#define NO_TURN_LEFT 0
#define TURN_LEFT 1
#define TURN_RIGHT 3
#define MAX_DISTANCE_FROM_BASE 800
#define BATTERY_LOW 30
#define BATTERY_VERY_LOW 15

#endif
