/**
 * Coordinates needed by other systems
 */

#ifndef COORDINATES_H
#define COORDINATES_H

#include "stdint.h"

/**
 * Coordinates (x,y,z) needed by other Crazyflie systems for positionning and mapping purposes
 */
typedef struct COORDINATES Coordinates;
struct COORDINATES
{
    int16_t x;
    int16_t y;
    int16_t z;
};

#endif

