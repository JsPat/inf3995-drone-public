/**
 * This file contains all necessary struct and enum for the messages for the
 * communication between drones and base station
 * 
 */

#ifndef MESSAGE_H
#define MESSAGE_H


// Messages are 27bytes, header are 5bytes and body is 22bytes
#define MESSAGE_LENGTH 27
#define MESSAGE_HEADER_LENGTH 5
#define MESSAGE_BODY_LENGTH 22

// ID
#define ID_BASE_STATION 0
#define ID_BROADCAST 255

// Position in message buffer
#define POSITION_MESSAGE_ID_0 0
#define POSITION_MESSAGE_ID_1 1
#define POSITION_MESSAGE_SOURCE_ID 2
#define POSITION_MESSAGE_DESTINATION_ID 3
#define POSITION_MESSAGE_TYPE 4


/**
 * Type of messages for drone communication
 * @constant INFO_MAPPING : contains position, battery level, current state,
 *                          etc and mapping info from environment
 * @constant COMMAND : contains a command from base station to drone
 */
enum MessageType
{
    INFO_MAPPING = 0,
    COMMAND,
};


/**
 * 0    H   messageId       (uint16_t)
 * 1    H   .
 * 2    H   sourceId        (uint8_t)
 * 3    H   destinationId   (uint8_t)
 * 4    H   messageType     (uint8_t)
 * 5    droneState          (uint8_t)
 * 6    batteryLevel        (uint8_t)
 * 7    distanceFront       (uint16_t)
 * 8    .
 * 9    distanceLeft        (uint16_t)
 * 10   .
 * 11   distanceBack        (uint16_t)
 * 12   .
 * 13   distanceRight       (uint16_t)
 * 14   .
 * 15   droneSpeedX         (int16_t)
 * 16   .
 * 17   droneSpeedY         (int16_t)
 * 18   .
 * 19   droneSpeedZ         (int16_t)
 * 20   .
 * 21   dronePositionX      (int16_t)
 * 22   .
 * 23   dronePositionY      (int16_t)
 * 24   .
 * 25   dronePositionZ      (int16_t)
 * 26   .
 * 27   
 * 28   
 * 29   
 */

/**
 * Header of messages for drone communication
 * @field messageId (ID of the message from the drone)
 * @field sourceId : {base: 0, drone: any other int}
 * @field destinationId : {base: 0, broadcast: 255, drone: any other int}
 * @field messageType : {INFO_MAPPING = 0, COMMAND = 1}
 */
typedef struct MESSAGE_HEADER MessageHeader;
struct MESSAGE_HEADER
{
    uint16_t messageId;
    uint8_t sourceId;
    uint8_t destinationId;
    uint8_t messageType;
}  __attribute__((packed));


/**
 * Body of messages for drone communication
 * @field droneState : see available states in common/state.h
 * @field batteryLevel
 * @field distanceFront
 * @field distanceLeft
 * @field distanceBack
 * @field distanceRight
 * @field droneSpeedX
 * @field droneSpeedY
 * @field droneSpeedZ
 * @field dronePositionX
 * @field dronePositionY
 * @field dronePositionZ
 */
typedef struct MESSAGE_BODY MessageBody;
struct MESSAGE_BODY
{
    uint8_t droneState;
    uint8_t batteryLevel;
    uint16_t distanceFront;
    uint16_t distanceLeft;
    uint16_t distanceBack;
    uint16_t distanceRight;
    int16_t droneSpeedX;
    int16_t droneSpeedY;
    int16_t droneSpeedZ;
    int16_t dronePositionX;
    int16_t dronePositionY;
    int16_t dronePositionZ;
}  __attribute__((packed));

/**
 * Message between drones and base station
 * @field header
 * @field body
 */
typedef struct MESSAGE Message;
struct MESSAGE
{
    MessageHeader header;
    MessageBody body;
} __attribute__((packed));


/**
 * Command message sent by station to drones
 * @field header
 * @field body
 */
typedef struct MESSAGE_COMMAND MessageCommand;
struct MESSAGE_COMMAND
{
    MessageHeader header;
    uint8_t body[MESSAGE_BODY_LENGTH];
} __attribute__((packed));

/**
 * Type of commands the base send for drone
 * @constant COMMAND_NO_COMMAND
 * @constant COMMAND_STANDBY
 * @constant COMMAND_LED
 * @constant COMMAND_TAKEOFF
 * @constant COMMAND_LAND
 * @constant COMMAND_RETURN_TO_BASE
 * @constant COMMAND_UPDATE
 * @constant NUMBER_OF_COMMANDS
 */
enum CommandType
{
    COMMAND_NO_COMMAND = -1,
    COMMAND_STANDBY = 0,
    COMMAND_LED,
    COMMAND_TAKEOFF,
    COMMAND_LAND,
    COMMAND_RETURN_TO_BASE,
    COMMAND_UPDATE,
    NUMBER_OF_COMMANDS,
};

#endif
