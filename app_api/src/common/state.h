/**
 * All possible states of the Crazyflie drone
 */


#ifndef STATE_H
#define STATE_H

/**
 * State of the Crazyflie drone
 */
enum State
{
    STATE_STANBY = 0,
    STATE_TAKEOFF,
    STATE_IN_MISSION,
    STATE_LANDING,
    STATE_NEED_RECHARGE,
    STATE_CHARGING,
    STATE_UPDATING,
    STATE_CRASHED,
    STATE_RETURN_TO_BASE,
};

#endif

