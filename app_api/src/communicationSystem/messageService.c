/**
 * This file contains the message service
 * 
 * Inspired from appchannel_test.c
 * this file can be found here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_appchannel_test/src/appchannel_test.c
 * 
 * P2P communication inspired from here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_peer_to_peer/src/peer_to_peer.c
 */


#include "messageService.h"


uint16_t currentMessageId = 0;
uint8_t droneId = 1;


void initializeCommunication() {
    droneId = (uint8_t)configblockGetRadioAddress();
}

Message generateMessage(uint8_t destinationId, uint8_t messageType, MessageBody body) {
    MessageHeader header = {
        .messageId = currentMessageId++,
        .sourceId = droneId,
        .destinationId = destinationId,
        .messageType = messageType,
    };
    Message message = {
        .header = header,
        .body = body,
    };
    return message;
}

enum CommandType receiveMessage() {
    command = COMMAND_NO_COMMAND;
    if (appchannelReceivePacket(&bufferMessageReceived, sizeof(bufferMessageReceived), TIMEOUT_MESSAGE_RECEIVE)) {
        command = readBufferMessageReceived();
    }
    return command;
}

void sendMessage(Message message) {
    // This implementation is ugly but necessary, because members of struct are of different
    // sizes and we want to fit every member in a communication packet to send to server

    uint8_t positionInBuffer = 0;
    // Header
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.header.messageId & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.header.messageId >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)message.header.sourceId;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)message.header.destinationId;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)message.header.messageType;
    // Body
    bufferMessageToSend[positionInBuffer++] = (uint8_t)message.body.droneState;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)message.body.batteryLevel;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceFront & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceFront >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceLeft & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceLeft >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceBack & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceBack >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceRight & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.distanceRight >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedX & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedX >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedY & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedY >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedZ & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.droneSpeedZ >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionX & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionX >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionY & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionY >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionZ & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(message.body.dronePositionZ >> 8);
    appchannelSendPacket(bufferMessageToSend, MESSAGE_LENGTH);
    sendMessageP2P();
}

void sendInfoMappingMessage(uint8_t state) {
    positionSensor = getCurrentPositionDrone();
    distanceSensor = getCurrentDistancesDrone();
    lastPositionSensor = getLastPositionDrone();
    MessageBody body = {
        .droneState = state,
        .batteryLevel = getBatteryLevel(),
        .distanceFront = distanceSensor.front,
        .distanceLeft = distanceSensor.left,
        .distanceBack = distanceSensor.back,
        .distanceRight = distanceSensor.right,
        .droneSpeedX = (positionSensor.x - lastPositionSensor.x) * MESSAGE_SPEED_RATIO,
        .droneSpeedY = (positionSensor.y - lastPositionSensor.y) * MESSAGE_SPEED_RATIO,
        .droneSpeedZ = (positionSensor.z - lastPositionSensor.z) * MESSAGE_SPEED_RATIO,
        .dronePositionX = positionSensor.x,
        .dronePositionY = positionSensor.y,
        .dronePositionZ = positionSensor.z,
    };
    sendMessage(generateMessage(ID_BROADCAST, INFO_MAPPING, body));
}

void sendMessageP2P() {
    // Initialize the P2P packet
    static P2PPacket packetToSend;
    packetToSend.port=0x00;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        packetToSend.data[i] = bufferMessageToSend[i];
    }
    packetToSend.size = MESSAGE_LENGTH;
    // Send the P2P packet
    radiolinkSendP2PPacketBroadcast(&packetToSend);
}

void p2pCallbackHandler(P2PPacket *p) {
    // NOTE: not really used in this program, only there to meet specifications
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = p->data[i];
    }
    command = readBufferMessageReceived();
}

enum CommandType readBufferMessageReceived() {
    command = COMMAND_NO_COMMAND;
    currentMessageReceived.header.messageId =
        (bufferMessageReceived[POSITION_MESSAGE_ID_0] << 8 | bufferMessageReceived[POSITION_MESSAGE_ID_1]);
    currentMessageReceived.header.sourceId = bufferMessageReceived[POSITION_MESSAGE_SOURCE_ID];
    currentMessageReceived.header.destinationId = bufferMessageReceived[POSITION_MESSAGE_DESTINATION_ID];
    currentMessageReceived.header.messageType = bufferMessageReceived[POSITION_MESSAGE_TYPE];

    // Message not for this drone so ignore it
    if (currentMessageReceived.header.destinationId != ID_BROADCAST
    && currentMessageReceived.header.destinationId != droneId) {
        return COMMAND_NO_COMMAND;
    }

    switch (currentMessageReceived.header.messageType)
    {
    case (uint8_t)INFO_MAPPING:
        // NOTE: not really used in this program, only there to meet specifications for P2P drone communication
        return COMMAND_LED;
        break;
    
    case (uint8_t)COMMAND:
        for (uint8_t bufferPosition = MESSAGE_HEADER_LENGTH; bufferPosition < MESSAGE_BODY_LENGTH; ++bufferPosition) {
            currentMessageReceived.body[bufferPosition - MESSAGE_HEADER_LENGTH] = bufferMessageReceived[bufferPosition];
        }
        // Only read first byte, because if the message is a command, only the first byte is useful
        if ((uint8_t)currentMessageReceived.body[0] >= NUMBER_OF_COMMANDS) {
            // Invalid command so return a COMMAND_NO_COMMAND
            return COMMAND_NO_COMMAND;
        }
        return currentMessageReceived.body[0];
        break;
    
    default:
        break;
    }
    return command;
}


