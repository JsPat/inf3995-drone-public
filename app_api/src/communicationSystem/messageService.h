/**
 * This file contains the message service
 * 
 * Inspired from appchannel_test.c
 * this file can be found here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_appchannel_test/src/appchannel_test.c
 * 
 * P2P communication inspired from here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_peer_to_peer/src/peer_to_peer.c
 */

#ifndef MESSAGE_SERVICE_H
#define MESSAGE_SERVICE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "app_channel.h"
#include "radiolink.h"
#include "configblock.h"

#include "../common/message.h"
#include "../common/state.h"
#include "../sensorSystem/positionSensor.h"
#include "../sensorSystem/batterySensor.h"

// Time to send and receive messages
#define TIMEOUT_MESSAGE_RECEIVE 250
#define TICK_FREQUENCY_MESSAGE_TO_SEND 250
// To compute drone speed
#define SEC_TO_MSEC 1000
#define MESSAGE_SPEED_RATIO (SEC_TO_MSEC/(TIMEOUT_MESSAGE_RECEIVE + TICK_FREQUENCY_MESSAGE_TO_SEND))


uint16_t currentMessageId;
uint8_t droneId;
enum CommandType command;

uint8_t bufferMessageReceived[MESSAGE_LENGTH];
uint8_t bufferMessageToSend[MESSAGE_LENGTH];
MessageCommand currentMessageReceived;

/**
 * Initialize everything needed for drone communication
 */
void initializeCommunication();

/**
 * Generates a message to send
 * @return the generated message
 */
Message generateMessage(uint8_t destinationId, uint8_t messageType, MessageBody body);

/**
 * Received a message and send the command received
 * @return the received command
 */
enum CommandType receiveMessage();

/**
 * Sends a message
 */
void sendMessage(Message message);

/**
 * Sends info and mapping message to station and other drones
 */
void sendInfoMappingMessage(uint8_t state);

/**
 * Sends P2P messages between drones
 * 
 * Inspired from here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_peer_to_peer/src/peer_to_peer.c
 */
void sendMessageP2P();

/**
 * Receives P2P messages from other drones
 * 
 * Inspired from here: https://github.com/bitcraze/crazyflie-firmware/blob/master/examples/app_peer_to_peer/src/peer_to_peer.c
 * 
 * NOTE: not really used in this program, only there to meet specifications
 */
void p2pCallbackHandler(P2PPacket *p);

/**
 * Read the buffer received and return the corresponding command
 * @return the received command
 */
enum CommandType readBufferMessageReceived();

#endif
