/**
 * To control the entire flying system and dispatch commands
 * from the communication system to the flying system.
 * 
 * 
 * Inspired from the push.c example that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#include "controlUnit.h"


bool setMissionInit = false;
bool setReturnToBaseInit = false;

void turnRight() {
    for(uint8_t i = 0; i < RIGHTSIDE; i++){
        side++;
        if(side > RIGHTSIDE){
            side = FRONTSIDE;
        }
    }
}

void turnLeft() {
    side++;
    if(side > RIGHTSIDE){
        side = FRONTSIDE;
    }
}

void changeSide() {
    getSensorsAndPosition();
    bool sideAvailable [MAX_SENSORS];
    sideAvailable[FRONTSIDE] = (distanceSensor.front >= (uint16_t)RADIUS_FOR_TURN);
    sideAvailable[LEFTSIDE] = (distanceSensor.left >= (uint16_t)RADIUS_FOR_TURN);
    sideAvailable[BACKSIDE] = (distanceSensor.back >= (uint16_t)RADIUS_FOR_TURN);
    sideAvailable[RIGHTSIDE] = (distanceSensor.right >= (uint16_t)RADIUS_FOR_TURN);
    uint8_t nbSideAvailable = NO_SIDE_AVAILABLE;
    if(isReturnToBase) {
        turnRight();
        if(turningLeft >= MAX_SENSORS){
            turningLeft = NO_TURN_LEFT;
        }
        else if(turningLeft > NO_TURN_LEFT){
            turningLeft--;
        } 
    }
    else {
        uint8_t allAvailableSides[MAX_SENSORS];
        for (uint8_t i = 0; i < MAX_SENSORS; i++) {
            if(sideAvailable[i]){
                allAvailableSides[nbSideAvailable] = i;
                nbSideAvailable++;
            }
        }
        if (nbSideAvailable == NO_SIDE_AVAILABLE) {
            side = STOP;
        }
        else {
            srand((abs(position.x) * FIRST_RANDOM_SEED) % SECOND_RANDOM_SEED);
            side = allAvailableSides[rand() % nbSideAvailable];
        }
    }
}

bool stabilizeDrone() {
    float goRight = NO_SPEED, goLeft = NO_SPEED, goBack = NO_SPEED, goFront = NO_SPEED;
    
    if (distanceSensor.left < (uint16_t)RADIUS) {
        goRight = -FAST_SPEED;
        if (lastDistanceSensor.left <= distanceSensor.left) {
            goRight = NO_SPEED;
            if(side == LEFTSIDE) {
                turning = true;
                changeSide();
            }
        }
    }
    if (distanceSensor.right < (uint16_t)RADIUS) {
        goLeft = FAST_SPEED;
        if (lastDistanceSensor.right <= distanceSensor.right) {
            goLeft = NO_SPEED;
            if(side == RIGHTSIDE) {
                turning = true;
                changeSide();
            }
        }
    }
    if (distanceSensor.front < (uint16_t)RADIUS) {
        goBack = -FAST_SPEED;
        if (lastDistanceSensor.front <= distanceSensor.front) {
            goBack = NO_SPEED;
            if (side == FRONTSIDE) {
                turning = true;
                changeSide();
            }
        }
    }
    if (distanceSensor.back < (uint16_t)RADIUS) {
        goFront = FAST_SPEED;
        if (lastDistanceSensor.back <= distanceSensor.back) {
            goFront = NO_SPEED;
            if (side == BACKSIDE) {
                turning = true;
                changeSide();
            }
        }
    }

    float velocitySide = goLeft + goRight;
    float velocityFront = goFront + goBack;
    if (velocitySide == NO_SPEED && velocityFront == NO_SPEED) {
        return true;
    }
    setHoverSetpoint(&setpoint, velocityFront, velocitySide, HEIGHT_SP, NO_ROTATION);
    commanderSetSetpoint(&setpoint, PRIORITY_STABILIZATION);
    return false;
}

void getSensorsAndPosition() {
    distanceSensor = getCurrentDistancesDrone();
    lastDistanceSensor = getLastDistanceDrone();
    position = getCurrentPositionDrone();
}

void executeMissionStep() {
    getSensorsAndPosition();
    isReturnToBase = false;

    if (!setMissionInit) {
        turning = false;
        side = FRONTSIDE;
        setMissionInit = true;
    }
    bool droneStabilized = stabilizeDrone();

    if (droneStabilized && !turning) {
        moveDirection(side);
    } 
    if (turning && droneStabilized) {
        moveDirection(STOP);
        turning = false;
    }
    setHoverSetpoint(&setpoint, SPEED, NO_SPEED, HEIGHT_SP, NO_ROTATION);
    commanderSetSetpoint(&setpoint, PRIORITY_NON_BLOCKING_MOVE);
}

bool land() {
    getSensorsAndPosition();
    setHoverSetpoint(&setpoint, NO_SPEED, NO_SPEED, NO_SPEED, NO_ROTATION);
    commanderSetSetpoint(&setpoint, PRIORITY_LAND);
    if (position.z < LANDING_HEIGHT) {
        memset(&setpoint, 0, sizeof(setpoint_t));
        commanderSetSetpoint(&setpoint, PRIORITY_LAND);
        return true;
    }
    return false;
}

bool returnToBase() {
    getSensorsAndPosition();
    isReturnToBase = true;
    if (!setReturnToBaseInit) {
        side = LEFTSIDE;
        changeLeftSide = false;
        turningLeft = 0;
        setReturnToBaseInit = true;
    }
    bool sideAvailable [MAX_SENSORS];
    sideAvailable[FRONTSIDE] = (distanceSensor.front >= (uint16_t)RADIUS_RTB);
    sideAvailable[LEFTSIDE] = (distanceSensor.left >= (uint16_t)RADIUS_RTB);
    sideAvailable[BACKSIDE] = (distanceSensor.back >= (uint16_t)RADIUS_RTB);
    sideAvailable[RIGHTSIDE] = (distanceSensor.right >= (uint16_t)RADIUS_RTB);
    
    bool droneStabilized = stabilizeDrone();

    if (droneStabilized) {
        if (changeLeftSide) {
            moveDirection(side);
            if (!sideAvailable[checkSide((uint8_t)TURN_LEFT)]) {
                changeLeftSide = false;
            }
        }
        else if (sideAvailable[checkSide((uint8_t)TURN_LEFT)] && turningLeft < MAX_SENSORS) {
            turnLeft();
            changeLeftSide = true;
            turningLeft++;
        }
        else {
            moveDirection(side);
        }
    }
    return (abs(position.x) < MAX_DISTANCE_FROM_BASE && abs(position.y) < MAX_DISTANCE_FROM_BASE);
}

uint8_t checkSide(uint8_t nbTurn){
    uint8_t sideAvailableLeft = side;
    for(uint8_t i = 0; i < nbTurn; i++){
        sideAvailableLeft++;
        if(sideAvailableLeft > RIGHTSIDE){
            sideAvailableLeft = FRONTSIDE;
        }
    }
    return sideAvailableLeft;
}

