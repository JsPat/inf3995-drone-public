/**
 * To control the entire flying system and dispatch commands
 * from the communication system to the flying system.
 * 
 * 
 * Inspired from the push.c example that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#ifndef CONTROL_UNIT_H
#define CONTROL_UNIT_H

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "app.h"
#include "commander.h"
#include "FreeRTOS.h"
#include "task.h"
#include "log.h"
#include "param.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "../common/coordinates.h"
#include "../common/state.h"
#include "../sensorSystem/positionSensor.h"
#include "motorUnit.h"

#define RADIUS 200
#define RADIUS_FOR_TURN 600
#define RADIUS_RTB 800
#define FAST_SPEED 0.2f
#define FIRST_RANDOM_SEED 97
#define SECOND_RANDOM_SEED 23
#define LANDING_HEIGHT 5

DistanceSensor distanceSensor;
DistanceSensor lastDistanceSensor;
Coordinates position;
uint8_t side;
bool turning;

bool changeLeftSide;
uint8_t turningLeft;
bool isReturnToBase;

bool setMissionInit;
bool setReturnToBaseInit;

/**
 * To make the drone turn to its right
 */
void turnRight();

/**
 * To make the drone turn to its right
 */
void turnLeft();

/**
 * To make the drone change the side at which it's moving
 */
void changeSide();

/**
 * To stabilize the drone during flight
 */
bool stabilizeDrone();

/**
 * To get the distance sensors and position values  
 */
void getSensorsAndPosition();

/**
 * To execute a step of the mission
 */
void executeMissionStep();

/**
 * To make the drone land
 * @return true if drone has landed
 */
bool land();

/**
 * To make the drone return to its base
 * @return true if drone has returned
 */
bool returnToBase();

/**
 * To check other side
 * @return nbTurn added to the current side, after validation
 */
uint8_t checkSide(uint8_t nbTurn);

#endif

