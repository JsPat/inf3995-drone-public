/**
 * To control the motor system 
 * from control system.
 * 
 * 
 * Inspired from the push.c example that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#include "motorUnit.h"


void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate)
{
    setpoint->mode.z = modeAbs;
    setpoint->position.z = z;

    setpoint->mode.yaw = modeVelocity;
    setpoint->attitudeRate.yaw = yawrate;

    setpoint->mode.x = modeVelocity;
    setpoint->mode.y = modeVelocity;
    setpoint->velocity.x = vx;
    setpoint->velocity.y = vy;

    setpoint->velocity_body = true;
}

void moveDirection(int side){
    switch(side){
        case FRONTSIDE:
            setHoverSetpoint(&setpoint, SPEED, NO_SPEED, HEIGHT_SP, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
        case LEFTSIDE:
            setHoverSetpoint(&setpoint, NO_SPEED, SPEED, HEIGHT_SP, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
        case BACKSIDE:
            setHoverSetpoint(&setpoint, -SPEED, NO_SPEED, HEIGHT_SP, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
        case RIGHTSIDE:
            setHoverSetpoint(&setpoint, NO_SPEED, -SPEED, HEIGHT_SP, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
        case DOWNSIDE:
            setHoverSetpoint(&setpoint, NO_SPEED, NO_SPEED, NO_SPEED, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
        case STOP:
            setHoverSetpoint(&setpoint, NO_SPEED, NO_SPEED, HEIGHT_SP, NO_ROTATION);
            commanderSetSetpoint(&setpoint, PRIORITY_MOVE);
            break;
    }
}

