/**
 * To control the motor system 
 * from control system.
 * 
 * 
 * Inspired from the push.c example that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#ifndef MOTOR_UNIT_H
#define MOTOR_UNIT_H

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "app.h"
#include "commander.h"
#include "FreeRTOS.h"
#include "task.h"
#include "log.h"
#include "param.h"

#include "../common/controlConstants.h"
#include "priorityConstants.h"

#define NO_SPEED 0.0f
#define SPEED 0.1f
#define HEIGHT_SP 0.2f
#define NO_ROTATION 0.0f

setpoint_t setpoint;

/**
 * To move the drone so it hovers and move according to the velocities passed
 */
void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate);

/**
 * To move the drone at a specific side
 */
void moveDirection(int side);

#endif

