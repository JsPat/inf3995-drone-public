/**
 * All priority values for drone control
 */

#ifndef PRIORITY_CONSTANTS_H
#define PRIORITY_CONSTANTS_H

#define PRIORITY_NON_BLOCKING_MOVE 1 // To avoid getting blocked somewhere while moving
#define PRIORITY_MOVE 3
#define PRIORITY_STABILIZATION 4
#define PRIORITY_LAND 9


#endif


