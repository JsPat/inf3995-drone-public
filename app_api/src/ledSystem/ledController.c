/**
 * To control the LEDs on the Crazyflie drone
 * 
 * Inspired from app_ledseq.h and app_ledsq.c
 * from here: https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/app_api/src/
 * 
 */


#include "ledController.h"


const int TIME_LED_ON = 1000;
const int TIME_LED_OFF = 500;

static ledseqStep_t ledSequenceStep[] = {
    {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
    {true, LEDSEQ_WAITMS(TIME_LED_ON)},
    {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
    {true, LEDSEQ_WAITMS(TIME_LED_ON)},
    {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
    {true, LEDSEQ_WAITMS(TIME_LED_ON)},
    {false, LEDSEQ_STOP},
};


static ledseqContext_t ledSequenceContext = {
    .sequence = ledSequenceStep,
    .led = LED_GREEN_R,
};


bool turnOnLed() {
    ledseqRegisterSequence(&ledSequenceContext);
    bool sequenceStarted = ledseqRun(&ledSequenceContext);
    return sequenceStarted;
}


