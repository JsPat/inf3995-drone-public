/**
 * To control the LEDs on the Crazyflie drone
 * 
 * Inspired from app_ledseq.h and app_ledsq.c
 * from here: https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/app_api/src/
 * 
 */


#ifndef LED_CONTROLLER_H
#define LED_CONTROLLER_H

#include "ledseq.h"


/**
 * Turn ON the LED sequence
 * @return true if the LED is ON
 */
bool turnOnLed();

#endif
