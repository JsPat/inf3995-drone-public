/**
 * This file contains the battery level sensor
 * 
 */

#include "batterySensor.h"

uint8_t getBatteryLevel() {
    // Inspired from pm_stm32f4.c from firmware
    uint8_t charge = MIN_BATTERY;
    float voltage = pmGetBatteryVoltage();

    if (voltage <= bat671723HS25C[0])
    {
        return MIN_BATTERY;
    }
    else if (voltage >= bat671723HS25C[BATTERY_MEASURES_LENGHT - 1])
    {
        return MAX_BATTERY;
    }
    else {
        while (voltage >  bat671723HS25C[charge])
        {
            charge += 1;
        }
        charge = (charge * BATTERY_STEP) + BATTERY_STEP;
        // Make sure we don't read problematic charge values
        if (charge <= MIN_BATTERY) {
            charge = MIN_BATTERY;
        }
        if (charge >= MAX_BATTERY) {
            charge = MAX_BATTERY;
        }
    }
    return charge;
}
