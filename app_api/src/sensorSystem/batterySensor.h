/**
 * This file contains the battery level sensor
 * 
 */

#ifndef BATTERY_SENSOR_H
#define BATTERY_SENSOR_H

#include "pm.h"

#define MAX_BATTERY 90
#define MIN_BATTERY 0
#define BATTERY_STEP 5
#define BATTERY_MEASURES_LENGHT 18

/**
 * All important voltage levels (specific to drone battery)
 * Inspired from pm_stm32f4.c from firmware
 */
static const float bat671723HS25C[BATTERY_MEASURES_LENGHT] =
{
    3.00, // 00%
    3.78, // 10%
    3.80, // 15%
    3.83, // 20%
    3.85, // 25%
    3.87, // 30%
    3.88, // 35%
    3.89, // 40%
    3.90, // 45%
    3.92, // 50%
    3.94, // 55%
    3.96, // 60%
    3.98, // 65%
    4.00, // 70%
    4.02, // 75%
    4.04, // 80%
    4.07, // 85%
    4.10  // 90%
};

/**
 * Get the battery level percentage on drone
 * @return the battery level
 */
uint8_t getBatteryLevel();


#endif

