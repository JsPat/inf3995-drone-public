/**
 * This file contains all necessary struct for the distance sensor
 */

#ifndef DISTANCE_SENSOR_H
#define DISTANCE_SENSOR_H

#include "log.h"

/**
 * The distance between the drone and its environment
 */
typedef struct DISTANCE_SENSOR DistanceSensor;
struct DISTANCE_SENSOR
{
    uint16_t front;
    uint16_t left;
    uint16_t back;
    uint16_t right;
    uint16_t up;
    uint16_t down;
};

/**
 * ID of distance variable
 */
typedef struct DISTANCE_SENSOR_ID DistanceSensorId;
struct DISTANCE_SENSOR_ID
{
    logVarId_t idFront;
    logVarId_t idLeft;
    logVarId_t idBack;
    logVarId_t idRight;
    logVarId_t idUp;
    logVarId_t idDown;
};


#endif
