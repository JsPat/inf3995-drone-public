/**
 * This file contains the position sensor
 * 
 * Inspired from the push demo, that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#include "positionSensor.h"

int nbTicks = RESET_TICKS;

void initializePositionSensor() {
    distanceSensorId.idFront = logGetVarId("range", "front");
    distanceSensorId.idLeft = logGetVarId("range", "left");
    distanceSensorId.idBack = logGetVarId("range", "back");
    distanceSensorId.idRight = logGetVarId("range", "right");
    distanceSensorId.idUp = logGetVarId("range", "up");
    distanceSensorId.idDown = logGetVarId("range", "down");

    positionSensorId.idX = logGetVarId("stateEstimate", "x");
    positionSensorId.idY = logGetVarId("stateEstimate", "y");
    positionSensorId.idZ = logGetVarId("stateEstimate", "z");
    
    idPositioningDeck = paramGetVarId("deck", "bcFlow2");
    idMultiranger = paramGetVarId("deck", "bcMultiranger");
    positioningInit = paramGetUint(idPositioningDeck);
    multirangerInit = paramGetUint(idMultiranger);
}

DistanceSensor getCurrentDistancesDrone() {
    if(nbTicks++ >= NB_TICKS_BEFORE_GET){
        lastDistanceSensor.front = distanceSensor.front;
        lastDistanceSensor.back = distanceSensor.back;
        lastDistanceSensor.left = distanceSensor.left;
        lastDistanceSensor.right = distanceSensor.right;
        lastDistanceSensor.up = distanceSensor.up;
        lastDistanceSensor.down = distanceSensor.down;
        nbTicks = RESET_TICKS;
    }
    distanceSensor.front = logGetUint(distanceSensorId.idFront);
    distanceSensor.left = logGetUint(distanceSensorId.idLeft);
    distanceSensor.back = logGetUint(distanceSensorId.idBack);
    distanceSensor.right = logGetUint(distanceSensorId.idRight);
    distanceSensor.up = logGetUint(distanceSensorId.idUp);
    // distanceSensor.down = logGetUint(distanceSensorId.idDown);
    return distanceSensor;
}

DistanceSensor getLastDistanceDrone() {
    return lastDistanceSensor;
}

Coordinates getCurrentPositionDrone() {
    lastPositionSensor.x = positionSensor.x;
    lastPositionSensor.y = positionSensor.y;
    lastPositionSensor.z = positionSensor.z;

    float positionX = logGetFloat(positionSensorId.idX) * M_TO_MM_FLOAT;
    float positionY = logGetFloat(positionSensorId.idY) * M_TO_MM_FLOAT;
    float positionZ = logGetFloat(positionSensorId.idZ) * M_TO_MM_FLOAT;
    positionSensor.x = (int16_t)positionX;
    positionSensor.y = (int16_t)positionY;
    positionSensor.z = (positionZ < 0.0f)? 0 : (int16_t)positionZ;
    return positionSensor;
}

Coordinates getLastPositionDrone() {
    return lastPositionSensor;
}

