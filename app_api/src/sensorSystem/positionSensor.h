/**
 * This file contains the position sensor
 * 
 * Inspired from the push demo, that can be found here:
 * https://github.com/bitcraze/crazyflie-firmware/blob/1a4cc9acf3edefb3833b72e05095be9c8b0fc887/examples/demos/app_push_demo/src/push.c
 */

#ifndef POSITION_SENSOR_H
#define POSITION_SENSOR_H

#include "log.h"
#include "param.h"

#include "distanceSensorTypes.h"
#include "positionSensorTypes.h"
#include "../common/coordinates.h"

#define NB_TICKS_BEFORE_GET 5
#define RESET_TICKS 0
#define M_TO_MM_FLOAT 1000.0f

DistanceSensor distanceSensor;
DistanceSensor lastDistanceSensor;
DistanceSensorId distanceSensorId;

Coordinates positionSensor;
Coordinates lastPositionSensor;
PositionSensorId positionSensorId;

paramVarId_t idPositioningDeck;
paramVarId_t idMultiranger;

uint8_t positioningInit;
uint8_t multirangerInit;


/**
 * Initialize the position ID variables and decks
 */
void initializePositionSensor();

/**
 * Get the current distances from the drone's perspective
 * @return the current distances read around the drone
 */
DistanceSensor getCurrentDistancesDrone();

/**
 * Get the precedent distances from the drone's perspective
 * @return the last distances read around the drone
 */
DistanceSensor getLastDistanceDrone();

/**
 * Get the current position of a drone relative to it's starting position
 * @return the current position of the drone
 */
Coordinates getCurrentPositionDrone();

/**
 * Get the last position of a drone relative to it's starting position
 * @return the last position of the drone
 */
Coordinates getLastPositionDrone();


#endif
