/**
 * This file contains all necessary struct for the position sensor
 */

#ifndef POSITION_SENSOR_TYPES_H
#define POSITION_SENSOR_TYPES_H

#include "log.h"

/**
 * ID of position variable
 */
typedef struct POSITION_SENSOR_ID PositionSensorId;
struct POSITION_SENSOR_ID
{
    logVarId_t idX;
    logVarId_t idY;
    logVarId_t idZ;
};


#endif
