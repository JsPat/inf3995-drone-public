/** File under test messageService.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#include <stdlib.h>

#ifndef TEST_MESSAGE_SERVICE
#define TEST_MESSAGE_SERVICE


#include "../../app_api/src/communicationSystem/messageService.h"
#include "unity.h"
// #include "mock_messageService.h"
#include "mock_batterySensor.h"
#include "mock_positionSensor.h"
#include "mock_app.h"
#include "mock_app_channel.h"
#include "mock_deck.h"
#include "mock_radiolink.h"
#include "mock_configblock.h"

MessageHeader header = {
    .messageId = 0,
    .sourceId = 0,
    .destinationId = 0,
    .messageType = 0,
};
MessageBody body = {
    .droneState = 0,
    .batteryLevel = 0,
    .distanceFront = 0,
    .distanceLeft = 0,
    .distanceBack = 0,
    .distanceRight = 0,
    .droneSpeedX = 0,
    .droneSpeedY = 0,
    .droneSpeedZ = 0,
    .dronePositionX = 0,
    .dronePositionY = 0,
    .dronePositionZ = 0,
};

static enum State state = STATE_STANBY;

/**
 * Reset variables before every test
 */
void setUp(void)
{
    currentMessageId = 0;
    droneId = 1;
    bufferMessageReceived[0] = 0;
    bufferMessageReceived[1] = 0;
    bufferMessageReceived[2] = 0;
    bufferMessageReceived[3] = droneId;
    bufferMessageReceived[4] = COMMAND;
}


/**********************************************************
 * Tests initializeCommunication()
 *********************************************************/

void test_initializeCommunication_calls_configblockGetRadioAddress() {
    // Fixture
    uint8_t expected = 69;

    // Test
    configblockGetRadioAddress_ExpectAndReturn(expected);
    initializeCommunication();
    
    // Assert
    // Should pass if expected function is called
}

void test_initializeCommunication_modifies_droneId_with_radio_address_id() {
    // Fixture
    uint8_t expected = 69;
    droneId = 0;

    // Test
    configblockGetRadioAddress_ExpectAndReturn(expected);
    initializeCommunication();
    uint8_t actual = droneId;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_initializeCommunication_modifies_droneId_with_radio_address_id_even_if_not_uint8() {
    // Fixture
    uint32_t notExpected = 0xFFAABBCC;
    uint8_t expected = (uint8_t)0xFFAABBCC;
    droneId = 0;

    // Test
    configblockGetRadioAddress_ExpectAndReturn(expected);
    initializeCommunication();
    uint8_t actual = droneId;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
    TEST_ASSERT_NOT_EQUAL(notExpected, actual);
}



/**********************************************************
 * Tests generateMessage()
 *********************************************************/

void test_generateMessage_returns_message() {
    // Fixture

    // Test
    Message* actual = malloc(sizeof(uint8_t) * MESSAGE_LENGTH);
    *actual = generateMessage(ID_BROADCAST, INFO_MAPPING, body);
    
    // Assert
    TEST_ASSERT_NOT_NULL(actual);
}

void test_generateMessage_returns_message_with_good_parameters() {
    // Fixture
    uint8_t expectedDestinationId = 69;
    uint8_t expectedMessageType = 69;
    MessageBody expectedBody = {
        .droneState = 69,
        .batteryLevel = 69,
        .distanceFront = 69,
        .distanceLeft = 69,
        .distanceBack = 69,
        .distanceRight = 69,
        .droneSpeedX = 69,
        .droneSpeedY = 69,
        .droneSpeedZ = 69,
        .dronePositionX = 69,
        .dronePositionY = 69,
        .dronePositionZ = 69,
    };

    // Test
    Message* actual = malloc(sizeof(uint8_t) * MESSAGE_LENGTH);
    *actual = generateMessage(expectedDestinationId, expectedMessageType, expectedBody);
    
    // Assert
    TEST_ASSERT_EQUAL(expectedDestinationId, actual->header.destinationId);
    TEST_ASSERT_EQUAL(expectedMessageType, actual->header.messageType);
    TEST_ASSERT_EQUAL(expectedBody.droneState, actual->body.droneState);
    TEST_ASSERT_EQUAL(expectedBody.batteryLevel, actual->body.batteryLevel);
    TEST_ASSERT_EQUAL(expectedBody.distanceFront, actual->body.distanceFront);
    TEST_ASSERT_EQUAL(expectedBody.distanceLeft, actual->body.distanceLeft);
    TEST_ASSERT_EQUAL(expectedBody.distanceBack, actual->body.distanceBack);
    TEST_ASSERT_EQUAL(expectedBody.distanceRight, actual->body.distanceRight);
    TEST_ASSERT_EQUAL(expectedBody.droneSpeedX, actual->body.droneSpeedX);
    TEST_ASSERT_EQUAL(expectedBody.droneSpeedY, actual->body.droneSpeedY);
    TEST_ASSERT_EQUAL(expectedBody.droneSpeedZ, actual->body.droneSpeedZ);
    TEST_ASSERT_EQUAL(expectedBody.dronePositionX, actual->body.dronePositionX);
    TEST_ASSERT_EQUAL(expectedBody.dronePositionY, actual->body.dronePositionY);
    TEST_ASSERT_EQUAL(expectedBody.dronePositionZ, actual->body.dronePositionZ);
}

void test_generateMessage_increments_currentMessageId() {
    // Fixture
    currentMessageId = 0;
    uint16_t expected = 1;

    // Test
    generateMessage(ID_BROADCAST, INFO_MAPPING, body);
    uint16_t actual = currentMessageId;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests receiveMessage()
 *********************************************************/

void test_receiveMessage_calls_appchannelReceivePacket() {
    // Fixture
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }

    // Test
    appchannelReceivePacket_ExpectAndReturn(bufferMessageReceived, MESSAGE_LENGTH, TIMEOUT_MESSAGE_RECEIVE, MESSAGE_LENGTH);
    receiveMessage();

    // Assert
    // Should pass if expected function is called
}

void test_receiveMessage_returns_command_from_readBufferMessageReceived() {
    // Fixture
    enum CommandType expected = COMMAND_UPDATE;
    currentMessageReceived.body[0] = 0;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }
    bufferMessageReceived[4] = COMMAND;
    bufferMessageReceived[5] = COMMAND_UPDATE;

    // Test
    appchannelReceivePacket_ExpectAndReturn(bufferMessageReceived, MESSAGE_LENGTH, TIMEOUT_MESSAGE_RECEIVE, MESSAGE_LENGTH);
    enum CommandType actual = receiveMessage();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_receiveMessage_returns_COMMAND_NO_COMMAND_if_message_not_received() {
    // Fixture
    enum CommandType expected = COMMAND_NO_COMMAND;

    // Test
    appchannelReceivePacket_ExpectAndReturn(bufferMessageReceived, MESSAGE_LENGTH, TIMEOUT_MESSAGE_RECEIVE, 0);
    enum CommandType actual = receiveMessage();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests sendMessage()
 *********************************************************/

void test_sendMessage_modifiy_buffer_to_send() {
    // Fixture
    Message message = {
        .header = header,
        .body = body,
    };
    uint8_t expectedValue = 69;
    message.body.droneState = expectedValue;

    // Test
    appchannelSendPacket_Ignore();
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendMessage(message);
    
    // Assert
    uint8_t actualValue = bufferMessageToSend[5];
    TEST_ASSERT_EQUAL(expectedValue, actualValue);
}

void test_sendMessage_calls_appchannelSendPacket() {
    // Fixture
    Message message = {
        .header = header,
        .body = body,
    };
    uint8_t buffer[MESSAGE_LENGTH];
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        buffer[i] = 0;
    }

    // Test
    appchannelSendPacket_Expect(buffer, MESSAGE_LENGTH);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendMessage(message);

    // Assert
    // Should pass if expected function is called
}


/**********************************************************
 * Tests sendInfoMappingMessage()
 *********************************************************/

void test_sendInfoMappingMessage_calls_getbatteryLevel() {
    // Fixture
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_ExpectAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorTest);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_calls_appchannelSendPacket_from_sendMessage() {
    // Fixture
    uint8_t data[MESSAGE_LENGTH];
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        data[i] = 0;
    }
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Expect(data, MESSAGE_LENGTH);
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorTest);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_calls_getCurrentDistancesDrone() {
    // Fixture
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensorTest);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_calls_getCurrentPositionDrone() {
    // Fixture
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorTest);
    getCurrentPositionDrone_ExpectAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_calls_getLastPositionDrone() {
    // Fixture
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorTest);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_ExpectAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_calls_radiolinkSendP2PPacketBroadcast_from_sendMessageP2P() {
    // Fixture
    DistanceSensor distanceSensorTest;
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;
    static P2PPacket packetToSend;
    packetToSend.port=0x00;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        packetToSend.data[i] = bufferMessageToSend[i];
    }
    packetToSend.size = MESSAGE_LENGTH;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorTest);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_ExpectAndReturn(&packetToSend, 0);
    sendInfoMappingMessage(state);
    
    // Assert
    // Should pass if expected function is called
}

void test_sendInfoMappingMessage_modify_distanceSensor() {
    // Fixture
    DistanceSensor distanceSensorExpected = {
        .front = 69,
        .left = 69,
        .back = 69,
        .right = 69,
    };
    Coordinates positionSensorTest;
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensorExpected);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorTest);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);

    // Assert
    TEST_ASSERT_EQUAL(distanceSensorExpected.front, distanceSensor.front);
    TEST_ASSERT_EQUAL(distanceSensorExpected.left, distanceSensor.left);
    TEST_ASSERT_EQUAL(distanceSensorExpected.back, distanceSensor.back);
    TEST_ASSERT_EQUAL(distanceSensorExpected.right, distanceSensor.right);
}

void test_sendInfoMappingMessage_modify_positionSensor() {
    // Fixture
    DistanceSensor distanceSensorExpected;
    Coordinates positionSensorExpected = {
        .x = 69,
        .y = 69,
        .z = 69,
    };
    Coordinates lastPositionSensorTest;

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorExpected);
    getCurrentPositionDrone_ExpectAndReturn(positionSensorExpected);
    getLastPositionDrone_IgnoreAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);

    // Assert
    TEST_ASSERT_EQUAL(positionSensorExpected.x, positionSensor.x);
    TEST_ASSERT_EQUAL(positionSensorExpected.y, positionSensor.y);
    TEST_ASSERT_EQUAL(positionSensorExpected.z, positionSensor.z);
}

void test_sendInfoMappingMessage_modify_droneSpeed_to_mm_per_sec() {
    // Fixture
    DistanceSensor distanceSensorExpected;
    int16_t value1 = 420;
    int16_t value2 = 69;
    int16_t expected = (value1 - value2) * MESSAGE_SPEED_RATIO;
    Coordinates positionSensorExpected = {
        .x = value1,
        .y = value1,
        .z = value1,
    };
    Coordinates lastPositionSensorTest = {
        .x = value2,
        .y = value2,
        .z = value2,
    };

    // Test
    getBatteryLevel_IgnoreAndReturn(0);
    appchannelSendPacket_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensorExpected);
    getCurrentPositionDrone_IgnoreAndReturn(positionSensorExpected);
    getLastPositionDrone_ExpectAndReturn(lastPositionSensorTest);
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendInfoMappingMessage(state);

    int16_t actualX = ((uint16_t)bufferMessageToSend[16] << 8) | bufferMessageToSend[15];
    int16_t actualY = ((uint16_t)bufferMessageToSend[18] << 8) | bufferMessageToSend[17];
    int16_t actualZ = ((uint16_t)bufferMessageToSend[20] << 8) | bufferMessageToSend[19];

    // Assert
    TEST_ASSERT_EQUAL(expected, actualX);
    TEST_ASSERT_EQUAL(expected, actualY);
    TEST_ASSERT_EQUAL(expected, actualZ);
}


/**********************************************************
 * Tests sendMessageP2P()
 *********************************************************/

void test_sendMessageP2P_calls_radiolinkSendP2PPacketBroadcast() {
    // Fixture

    // Test
    radiolinkSendP2PPacketBroadcast_IgnoreAndReturn(0);
    sendMessageP2P();

    // Assert
    // Can't test because only local variable and function called with local variable
    TEST_ASSERT_EQUAL(true, true);
}



/**********************************************************
 * Tests p2pCallbackHandler()
 *********************************************************/

void test_p2pCallbackHandler_modifies_bufferMessageReceived() {
    // Fixture
    uint8_t expected = 69;
    static P2PPacket packet;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        packet.data[i] = expected;
    }

    // Test
    p2pCallbackHandler(&packet);

    // Assert
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        TEST_ASSERT_EQUAL(expected, bufferMessageReceived[i]);
    }
}



/**********************************************************
 * Tests readBufferMessageReceived()
 *********************************************************/

void test_readBufferMessageReceived_modifies_header_of_currentMessagereceived() {
    // Fixture
    uint8_t expected = 69;
    currentMessageReceived.header.messageId = 0;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = expected;
    }

    // Test
    readBufferMessageReceived();
    uint8_t actual = currentMessageReceived.header.messageId;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_modifies_body_of_currentMessagereceived_if_command_message() {
    // Fixture
    uint8_t expected = 69;
    bufferMessageReceived[4] = COMMAND;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = expected;
    }

    // Test
    readBufferMessageReceived();
    uint8_t actual = currentMessageReceived.body[0];
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_NO_COMMAND_if_message_not_for_this_drone_and_not_broadcast() {
    // Fixture
    enum CommandType expected = COMMAND_NO_COMMAND;
    currentMessageReceived.body[0] = 0;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 69;
    }

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_LED_if_INFO_MAPPING_message() {
    // Fixture
    enum CommandType expected = COMMAND_LED;
    currentMessageReceived.body[0] = 0;
    bufferMessageReceived[3] = droneId;
    bufferMessageReceived[4] = INFO_MAPPING;

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_NO_COMMAND_if_invalid_command() {
    // Fixture
    enum CommandType expected = COMMAND_NO_COMMAND;
    currentMessageReceived.body[0] = 0;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 69;
    }

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_UPDATE_if_COMMAND_UPDATE() {
    // Fixture
    enum CommandType expected = COMMAND_UPDATE;
    currentMessageReceived.body[0] = 0;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }
    bufferMessageReceived[4] = COMMAND;
    bufferMessageReceived[5] = COMMAND_UPDATE;

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_UPDATE_if_COMMAND_UPDATE_and_broadcast() {
    // Fixture
    enum CommandType expected = COMMAND_UPDATE;
    currentMessageReceived.body[0] = 0;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }
    bufferMessageReceived[3] = ID_BROADCAST;
    bufferMessageReceived[4] = COMMAND;
    bufferMessageReceived[5] = COMMAND_UPDATE;

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_RETURN_TO_BASE_if_COMMAND_RETURN_TO_BASE() {
    // Fixture
    enum CommandType expected = COMMAND_RETURN_TO_BASE;
    currentMessageReceived.body[0] = 0;
    for (int i = MESSAGE_HEADER_LENGTH; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }
    bufferMessageReceived[4] = COMMAND;
    bufferMessageReceived[5] = COMMAND_RETURN_TO_BASE;

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_readBufferMessageReceived_returns_COMMAND_NO_COMMAND_if_NUMBER_OF_COMMANDS() {
    // Fixture
    enum CommandType expected = COMMAND_NO_COMMAND;
    currentMessageReceived.body[0] = 0;
    for (int i = 0; i < MESSAGE_LENGTH; ++i) {
        bufferMessageReceived[i] = 0;
    }
    bufferMessageReceived[4] = COMMAND;
    bufferMessageReceived[5] = NUMBER_OF_COMMANDS;

    // Test
    enum CommandType actual = readBufferMessageReceived();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}


#endif
