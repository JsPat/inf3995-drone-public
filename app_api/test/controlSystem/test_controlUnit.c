/** File under test controlUnit.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#include <stdlib.h>

#ifndef TEST_CONTROL_UNIT
#define TEST_CONTROL_UNIT


#include "../../app_api/src/controlSystem/motorUnit.h"
#include "../../app_api/src/controlSystem/controlUnit.h"
#include "unity.h"
#include "mock_commander.h"
#include "mock_positionSensor.h"

/**
 * Reset variables before every test
 */
void setUp(void)
{
    distanceSensor.front = 0;
    distanceSensor.left = 0;
    distanceSensor.back = 0;
    distanceSensor.right = 0;
    distanceSensor.up = 0;
    distanceSensor.down = 0;

    lastDistanceSensor.front = 0;
    lastDistanceSensor.left = 0;
    lastDistanceSensor.back = 0;
    lastDistanceSensor.right = 0;
    lastDistanceSensor.up = 0;
    lastDistanceSensor.down = 0;

    position.x = 0;
    position.y = 0;
    position.z = 0;

    side = 0;
    turning = false;
    changeLeftSide = false;
    turningLeft = 0;
    isReturnToBase = false;
}



/**********************************************************
 * Tests turnRight()
 *********************************************************/

void test_turnRight_increments_side_to_RIGHTSIDE_if_FRONTSIDE() {
    // Fixture
    int expected = RIGHTSIDE;
    side = FRONTSIDE;

    // Test
    turnRight();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnRight_increments_side_to_FRONTSIDE_if_LEFTSIDE() {
    // Fixture
    int expected = FRONTSIDE;
    side = LEFTSIDE;

    // Test
    turnRight();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnRight_increments_side_to_LEFTSIDE_if_BACKSIDE() {
    // Fixture
    int expected = LEFTSIDE;
    side = BACKSIDE;

    // Test
    turnRight();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnRight_increments_side_to_BACKSIDE_if_RIGHTSIDE() {
    // Fixture
    int expected = BACKSIDE;
    side = RIGHTSIDE;

    // Test
    turnRight();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

/**********************************************************
 * Tests turnLeft()
 *********************************************************/

void test_turnLeft_increments_side_to_LEFTSIDE_if_FRONTSIDE() {
    // Fixture
    int expected = LEFTSIDE;
    side = FRONTSIDE;

    // Test
    turnLeft();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnLeft_increments_side_to_BACKSIDE_if_LEFTSIDE() {
    // Fixture
    int expected = BACKSIDE;
    side = LEFTSIDE;

    // Test
    turnLeft();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnLeft_increments_side_to_RIGHTSIDE_if_BACKSIDE() {
    // Fixture
    int expected = RIGHTSIDE;
    side = BACKSIDE;

    // Test
    turnLeft();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_turnLeft_increments_side_to_FRONTSIDE_if_RIGHTSIDE() {
    // Fixture
    int expected = FRONTSIDE;
    side = RIGHTSIDE;

    // Test
    turnLeft();
    int actual = side;
    
    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests changeSide()
 *********************************************************/

void test_changeSide_calls_getCurrentDistancesDrone_getLastDistanceDrone_getCurrentPositionDrone_from_getSensorsAndPosition() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    changeSide();

    // Assert
    // Should pass if expected function is called
}


void test_changeSide_calls_turnRight_if_isReturnToBase_is_true() {
    // Fixture
    int expected = RIGHTSIDE;
    isReturnToBase = true;
    side = FRONTSIDE;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_decrement_turningLeft_if_isReturnToBase_and_turningLeft_bigger_than_NO_TURN_LEFT_but_smaller_MAX_SENSORS() {
    // Fixture
    int expected = 3;
    isReturnToBase = true;
    turningLeft = expected;
    expected--;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_set_turningLeft_NO_TURN_LEFT_if_isReturnToBase_and_turningLeft_bigger_than_MAX_SENSORS() {
    // Fixture
    int expected = NO_TURN_LEFT;
    isReturnToBase = true;
    turningLeft = 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_first_available_side_if_isReturnToBase_false_FRONTSIDE() {
    // Fixture
    int expected = FRONTSIDE;
    isReturnToBase = false;
    distanceSensor.front = RADIUS_FOR_TURN + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_first_available_side_if_isReturnToBase_false_LEFTSIDE() {
    // Fixture
    int expected = LEFTSIDE;
    isReturnToBase = false;
    distanceSensor.left = RADIUS_FOR_TURN + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_first_available_side_if_isReturnToBase_false_BACKSIDE() {
    // Fixture
    int expected = BACKSIDE;
    isReturnToBase = false;
    distanceSensor.back = RADIUS_FOR_TURN + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_first_available_side_if_isReturnToBase_false_RIGHTSIDE() {
    // Fixture
    int expected = RIGHTSIDE;
    isReturnToBase = false;
    distanceSensor.right = RADIUS_FOR_TURN + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_STOP_if_isReturnToBase_false_and_no_side_available() {
    // Fixture
    int expected = STOP;
    isReturnToBase = false;
    distanceSensor.front = 0;
    distanceSensor.left = 0;
    distanceSensor.back = 0;
    distanceSensor.right = 0;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_changeSide_sets_side_to_random_side_available_if_2_sides_available() {
    // Fixture
    distanceSensor.front = RADIUS_FOR_TURN + 69;
    distanceSensor.left = RADIUS_FOR_TURN + 69;
    distanceSensor.back = 0;
    distanceSensor.right = 0;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_NOT_EQUAL(BACKSIDE, actual);
    TEST_ASSERT_NOT_EQUAL(RIGHTSIDE, actual);
}

void test_changeSide_sets_side_to_random_side_available_if_3_sides_available() {
    // Fixture
    distanceSensor.front = RADIUS_FOR_TURN + 69;
    distanceSensor.left = RADIUS_FOR_TURN + 69;
    distanceSensor.back = RADIUS_FOR_TURN + 69;
    distanceSensor.right = 0;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    changeSide();
    int actual = side;

    // Assert
    TEST_ASSERT_NOT_EQUAL(RIGHTSIDE, actual);
}



/**********************************************************
 * Tests stabilizeDrone()
 *********************************************************/


void test_stabilizeDrone_calls_getCurrentDistancesDrone_getLastDistanceDrone_getCurrentPositionDrone_from_changeSide() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    stabilizeDrone();

    // Assert
    // Should pass if expected function is called
}

void test_stabilizeDrone_return_true_if_velocitySide_and_velocityFront_is_NO_SPEED() {
    // Fixture
    bool expected = true;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    bool actual = stabilizeDrone();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_return_false_if_velocitySide_is_not_NO_SPEED() {
    // Fixture
    bool expected = false;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.right = 0;
    lastDistanceSensor.right = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    bool actual = stabilizeDrone();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_return_false_if_velocityFront_is_not_NO_SPEED() {
    // Fixture
    bool expected = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.back = 0;
    lastDistanceSensor.back = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    bool actual = stabilizeDrone();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_return_false_if_velocitySide_and_velocityFront_are_not_NO_SPEED() {
    // Fixture
    bool expected = false;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.right = 0;
    lastDistanceSensor.right = RADIUS + 69;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.back = 0;
    lastDistanceSensor.back = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    bool actual = stabilizeDrone();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_calls_commanderSetSetpoint_if_velocitySide_is_not_NO_SPEED() {
    // Fixture
    distanceSensor.left = RADIUS + 69;
    distanceSensor.right = 0;
    lastDistanceSensor.right = RADIUS + 69;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_STABILIZATION);
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();

    // Assert
    // Should pass if expected function is called
}

void test_stabilizeDrone_calls_commanderSetSetpoint_if_velocityFront_is_not_NO_SPEED() {
    // Fixture
    distanceSensor.front = RADIUS + 69;
    distanceSensor.back = 0;
    lastDistanceSensor.back = RADIUS + 69;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_STABILIZATION);
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();

    // Assert
    // Should pass if expected function is called
}

void test_stabilizeDrone_calls_commanderSetSetpoint_if_velocitySide_and_velocityFront_are_not_NO_SPEED() {
    // Fixture
    distanceSensor.left = RADIUS + 69;
    distanceSensor.right = 0;
    lastDistanceSensor.right = RADIUS + 69;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.back = 0;
    lastDistanceSensor.back = RADIUS + 69;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_STABILIZATION);
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();

    // Assert
    // Should pass if expected function is called
}

void test_stabilizeDrone_turning_true_if_small_distance_left_and_LEFTSIDE() {
    // Fixture
    bool expected = true;
    side = LEFTSIDE;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = 0;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;
    lastDistanceSensor.front = RADIUS + 69;
    lastDistanceSensor.left = 0;
    lastDistanceSensor.back = RADIUS + 69;
    lastDistanceSensor.right = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_turning_true_if_small_distance_right_and_RIGHTSIDE() {
    // Fixture
    bool expected = true;
    side = RIGHTSIDE;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = 0;
    lastDistanceSensor.front = RADIUS + 69;
    lastDistanceSensor.left = RADIUS + 69;
    lastDistanceSensor.back = RADIUS + 69;
    lastDistanceSensor.right = 0;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_turning_true_if_small_distance_front_and_FRONTSIDE() {
    // Fixture
    bool expected = true;
    side = FRONTSIDE;
    distanceSensor.front = 0;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;
    lastDistanceSensor.front = 0;
    lastDistanceSensor.left = RADIUS + 69;
    lastDistanceSensor.back = RADIUS + 69;
    lastDistanceSensor.right = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_stabilizeDrone_turning_true_if_small_distance_back_and_BACKSIDE() {
    // Fixture
    bool expected = true;
    side = BACKSIDE;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = 0;
    distanceSensor.right = RADIUS + 69;
    lastDistanceSensor.front = RADIUS + 69;
    lastDistanceSensor.left = RADIUS + 69;
    lastDistanceSensor.back = 0;
    lastDistanceSensor.right = RADIUS + 69;

    // Test
    commanderSetSetpoint_Ignore();
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    stabilizeDrone();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests getSensorsAndPosition()
 *********************************************************/

void test_getSensorsAndPosition_calls_getCurrentDistancesDrone() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    getSensorsAndPosition();

    // Assert
    // Should pass if expected function is called
}

void test_getSensorsAndPosition_calls_getLastDistanceDrone() {
    // Fixture

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    getSensorsAndPosition();

    // Assert
    // Should pass if expected function is called
}

void test_getSensorsAndPosition_calls_getCurrentPositionDrone() {
    // Fixture

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    getSensorsAndPosition();

    // Assert
    // Should pass if expected function is called
}



/**********************************************************
 * Tests executeMissionStep()
 *********************************************************/

void test_executeMissionStep_calls_getCurrentDistancesDrone_getLastDistanceDrone_getCurrentPositionDrone_from_getSensorsAndPosition() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    commanderSetSetpoint_Ignore();
    executeMissionStep();

    // Assert
    // Should pass if expected function is called
}

void test_executeMissionStep_calls_commanderSetSetpoint_from_moveDirection_if_turning_and_droneStabilized_true() {
    // Fixture

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_NON_BLOCKING_MOVE);
    executeMissionStep();

    // Assert
    // Should pass if expected function is called
}

void test_executeMissionStep_set_turning_false_if_setMissionInit_false() {
    // Fixture
    bool expected = false;
    setMissionInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    executeMissionStep();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_executeMissionStep_set_side_FRONTSIDE_if_setMissionInit_false() {
    // Fixture
    int expected = FRONTSIDE;
    setMissionInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    executeMissionStep();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_executeMissionStep_set_setMissionInit_true_if_setMissionInit_false() {
    // Fixture
    int expected = true;
    setMissionInit = false;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    executeMissionStep();
    int actual = setMissionInit;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_executeMissionStep_calls_commanderSetSetpoint_from_moveDirection_if_droneStabilized_true_and_turning_false() {
    // Fixture
    side = FRONTSIDE;
    setMissionInit = true;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_NON_BLOCKING_MOVE);
    executeMissionStep();

    // Assert
    // Should pass if expected function is called
}

void test_executeMissionStep_calls_commanderSetSetpoint_from_moveDirection_if_droneStabilized_true_and_turning_true() {
    // Fixture
    side = FRONTSIDE;
    setMissionInit = true;
    turning = true;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_NON_BLOCKING_MOVE);
    executeMissionStep();

    // Assert
    // Should pass if expected function is called
}

void test_executeMissionStep_sets_turning_false_if_droneStabilized_true_and_turning_true() {
    // Fixture
    bool expected = false;
    side = FRONTSIDE;
    setMissionInit = true;
    turning = true;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_NON_BLOCKING_MOVE);
    executeMissionStep();
    bool actual = turning;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests land()
 *********************************************************/

void test_land_calls_getCurrentDistancesDrone_getLastDistanceDrone_getCurrentPositionDrone_from_getSensorsAndPosition() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    commanderSetSetpoint_Ignore();
    land();

    // Assert
    // Should pass if expected function is called
}

void test_land_calls_commanderSetSetpoint_2_times_if_position_z_smaller_than_LANDING_HEIGHT() {
    // Fixture

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_LAND);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_LAND);
    land();

    // Assert
    // Should pass if expected function is called
}

void test_land_calls_commanderSetSetpoint_1_time_if_position_z_bigger_than_LANDING_HEIGHT() {
    // Fixture
    position.z = LANDING_HEIGHT + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_LAND);
    land();

    // Assert
    // Should pass if expected function is called
}

void test_land_return_false_if_position_z_bigger_than_LANDING_HEIGHT() {
    // Fixture
    bool expected = false;
    position.z = LANDING_HEIGHT + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    bool actual = land();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_land_return_true_if_position_z_smaller_than_LANDING_HEIGHT() {
    // Fixture
    bool expected = true;
    position.z = 0;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    bool actual = land();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests returnToBase()
 *********************************************************/

void test_returnToBase_calls_getCurrentDistancesDrone_getLastDistanceDrone_getCurrentPositionDrone_from_getSensorsAndPosition() {
    // Fixture

    // Test
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    getCurrentDistancesDrone_ExpectAndReturn(distanceSensor);
    getLastDistanceDrone_ExpectAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_ExpectAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();

    // Assert
    // Should pass if expected function is called
}

void test_returnToBase_calls_commanderSetSetpoint_from_moveDirection() {
    // Fixture

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    returnToBase();

    // Assert
    // Should pass if expected function is called
}

void test_returnToBase_sets_isReturnToBase_true() {
    // Fixture
    bool expected = true;
    isReturnToBase = false;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = isReturnToBase;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_side_FRONTSIDE_if_setReturnToBaseInit_false() {
    // Fixture
    int expected = LEFTSIDE;
    setReturnToBaseInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = side;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_changeLeftSide_false_if_setReturnToBaseInit_false() {
    // Fixture
    bool expected = false;
    setReturnToBaseInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = changeLeftSide;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_turningLeft_0_if_setReturnToBaseInit_false() {
    // Fixture
    int expected = 0;
    setReturnToBaseInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_setReturnToBaseInit_true_if_setReturnToBaseInit_false() {
    // Fixture
    bool expected = true;
    setReturnToBaseInit = false;
    distanceSensor.front = RADIUS + 69;
    distanceSensor.left = RADIUS + 69;
    distanceSensor.back = RADIUS + 69;
    distanceSensor.right = RADIUS + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = setReturnToBaseInit;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_changeLeftSide_false_if_droneStabilized_and_changeLeftSide_true_FRONTSIDE() {
    // Fixture
    bool expected = false;
    setReturnToBaseInit = true;
    changeLeftSide = true;
    side = FRONTSIDE;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = changeLeftSide;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_changeLeftSide_false_if_droneStabilized_and_changeLeftSide_true_LEFTSIDE() {
    // Fixture
    bool expected = false;
    setReturnToBaseInit = true;
    changeLeftSide = true;
    side = LEFTSIDE;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = changeLeftSide;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_changeLeftSide_false_if_droneStabilized_and_changeLeftSide_true_BACKSIDE() {
    // Fixture
    bool expected = false;
    setReturnToBaseInit = true;
    changeLeftSide = true;
    side = BACKSIDE;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = changeLeftSide;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_sets_changeLeftSide_false_if_droneStabilized_and_changeLeftSide_true_RIGHTSIDE() {
    // Fixture
    bool expected = false;
    setReturnToBaseInit = true;
    changeLeftSide = true;
    side = RIGHTSIDE;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    bool actual = changeLeftSide;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_increments_turningLeft_1_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_small() {
    // Fixture
    int expected = 0;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;
    expected++;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_increments_turningLeft_2_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_small() {
    // Fixture
    int expected = 1;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;
    expected++;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_increments_turningLeft_3_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_small() {
    // Fixture
    int expected = 2;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;
    expected++;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_increments_turningLeft_4_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_small() {
    // Fixture
    int expected = 3;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;
    expected++;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_turningLeft_stay_4_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_4() {
    // Fixture
    int expected = 4;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_turningLeft_stay_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_true_and_turningLeft_big() {
    // Fixture
    uint8_t expected = 69;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;
    side = FRONTSIDE;
    distanceSensor.front = RADIUS_RTB + 69;
    distanceSensor.left = RADIUS_RTB + 69;
    distanceSensor.back = RADIUS_RTB + 69;
    distanceSensor.right = RADIUS_RTB + 69;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_doesnt_increment_turningLeft_if_droneStabilized_true_and_changeLeftSide_false_and_sideAvailable_false() {
    // Fixture
    int expected = 0;
    turningLeft = expected;
    setReturnToBaseInit = true;
    changeLeftSide = false;

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(position);
    commanderSetSetpoint_Ignore();
    returnToBase();
    int actual = turningLeft;

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_return_true_if_abs_x_smaller_MAX_DISTANCE_FROM_BASE_and_abs_y_smaller_MAX_DISTANCE_FROM_BASE() {
    // Fixture
    bool expected = true;
    Coordinates expectedPosition = {
        .x = 69,
        .y = 69,
        .z = 69,
    };

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(expectedPosition);
    commanderSetSetpoint_Ignore();
    bool actual = returnToBase();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_return_false_if_abs_x_bigger_MAX_DISTANCE_FROM_BASE_and_abs_y_smaller_MAX_DISTANCE_FROM_BASE() {
    // Fixture
    bool expected = false;
    Coordinates expectedPosition = {
        .x = MAX_DISTANCE_FROM_BASE + 69,
        .y = 69,
        .z = 69,
    };

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(expectedPosition);
    commanderSetSetpoint_Ignore();
    bool actual = returnToBase();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_return_false_if_abs_x_smaller_MAX_DISTANCE_FROM_BASE_and_abs_y_bigger_MAX_DISTANCE_FROM_BASE() {
    // Fixture
    bool expected = false;
    Coordinates expectedPosition = {
        .x = 69,
        .y = MAX_DISTANCE_FROM_BASE + 69,
        .z = 69,
    };

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(expectedPosition);
    commanderSetSetpoint_Ignore();
    bool actual = returnToBase();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_return_false_if_abs_x_bigger_MAX_DISTANCE_FROM_BASE_and_abs_y_bigger_MAX_DISTANCE_FROM_BASE() {
    // Fixture
    bool expected = false;
    Coordinates expectedPosition = {
        .x = MAX_DISTANCE_FROM_BASE + 69,
        .y = MAX_DISTANCE_FROM_BASE + 69,
        .z = 69,
    };

    // Test
    getCurrentDistancesDrone_IgnoreAndReturn(distanceSensor);
    getLastDistanceDrone_IgnoreAndReturn(lastDistanceSensor);
    getCurrentPositionDrone_IgnoreAndReturn(expectedPosition);
    commanderSetSetpoint_Ignore();
    bool actual = returnToBase();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}



#endif
