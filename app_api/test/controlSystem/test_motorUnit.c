/** File under test motorUnit.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#include <stdlib.h>

#ifndef TEST_MOTOR_UNIT
#define TEST_MOTOR_UNIT


#include "../../app_api/src/controlSystem/motorUnit.h"
#include "unity.h"
#include "mock_app.h"
#include "mock_commander.h"

/**
 * Reset variables before every test
 */
void setUp(void)
{
    setpoint.mode.z = 0;
    setpoint.position.z = 0;
    setpoint.mode.yaw = 0;
    setpoint.attitudeRate.yaw = 0;
    setpoint.mode.x = 0;
    setpoint.mode.y = 0;
    setpoint.velocity.x = 0;
    setpoint.velocity.y = 0;
    setpoint.velocity_body = 0;
}



/**********************************************************
 * Tests setHoverSetpoint()
 *********************************************************/

void test_setHoverSetpoint_modifies_setpoint_positive_numbers() {
    // Fixture
    float expected = 69.0;

    // Test
    setHoverSetpoint(&setpoint, expected, expected, expected, expected);
    
    // Assert
    TEST_ASSERT_EQUAL(modeAbs, setpoint.mode.z);
    TEST_ASSERT_EQUAL(expected, setpoint.position.z);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.yaw);
    TEST_ASSERT_EQUAL(expected, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.x);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.y);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.y);
    TEST_ASSERT_EQUAL(true, setpoint.velocity_body);
}

void test_setHoverSetpoint_modifies_setpoint_negative_numbers() {
    // Fixture
    float expected = -69.0;

    // Test
    setHoverSetpoint(&setpoint, expected, expected, expected, expected);
    
    // Assert
    TEST_ASSERT_EQUAL(modeAbs, setpoint.mode.z);
    TEST_ASSERT_EQUAL(expected, setpoint.position.z);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.yaw);
    TEST_ASSERT_EQUAL(expected, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.x);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.y);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.y);
    TEST_ASSERT_EQUAL(true, setpoint.velocity_body);
}

void test_setHoverSetpoint_modifies_setpoint_small_numbers() {
    // Fixture
    float expected = 1.0;

    // Test
    setHoverSetpoint(&setpoint, expected, expected, expected, expected);
    
    // Assert
    TEST_ASSERT_EQUAL(modeAbs, setpoint.mode.z);
    TEST_ASSERT_EQUAL(expected, setpoint.position.z);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.yaw);
    TEST_ASSERT_EQUAL(expected, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.x);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.y);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.y);
    TEST_ASSERT_EQUAL(true, setpoint.velocity_body);
}

void test_setHoverSetpoint_modifies_setpoint_big_numbers() {
    // Fixture
    float expected = 6969696969.0;

    // Test
    setHoverSetpoint(&setpoint, expected, expected, expected, expected);
    
    // Assert
    TEST_ASSERT_EQUAL(modeAbs, setpoint.mode.z);
    TEST_ASSERT_EQUAL(expected, setpoint.position.z);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.yaw);
    TEST_ASSERT_EQUAL(expected, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.x);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.y);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.y);
    TEST_ASSERT_EQUAL(true, setpoint.velocity_body);
}

void test_setHoverSetpoint_modifies_setpoint_zero() {
    // Fixture
    float expected = 0.0;

    // Test
    setHoverSetpoint(&setpoint, expected, expected, expected, expected);
    
    // Assert
    TEST_ASSERT_EQUAL(modeAbs, setpoint.mode.z);
    TEST_ASSERT_EQUAL(expected, setpoint.position.z);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.yaw);
    TEST_ASSERT_EQUAL(expected, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.x);
    TEST_ASSERT_EQUAL(modeVelocity, setpoint.mode.y);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected, setpoint.velocity.y);
    TEST_ASSERT_EQUAL(true, setpoint.velocity_body);
}



/**********************************************************
 * Tests moveDirection()
 *********************************************************/

void test_moveDirection_calls_commanderSetSetpoint_if_FRONTSIDE() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(FRONTSIDE);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_calls_commanderSetSetpoint_if_LEFTSIDE() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(LEFTSIDE);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_calls_commanderSetSetpoint_if_BACKSIDE() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(BACKSIDE);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_calls_commanderSetSetpoint_if_RIGHTSIDE() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(RIGHTSIDE);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_calls_commanderSetSetpoint_if_DOWNSIDE() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(DOWNSIDE);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_calls_commanderSetSetpoint_if_STOP() {
    // Fixture

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(STOP);

    // Assert
    // Should pass if expected function is called
}

void test_moveDirection_doesnt_call_commanderSetSetpoint_if_greater_number_than_STOP() {
    // Fixture

    // Test
    moveDirection(6);

    // Assert
    // Should pass if expected function is not called
}

void test_moveDirection_doesnt_call_commanderSetSetpoint_if_negative_number() {
    // Fixture

    // Test
    moveDirection(-1);

    // Assert
    // Should pass if expected function is not called
}

void test_moveDirection_doesnt_call_commanderSetSetpoint_if_too_big_number() {
    // Fixture

    // Test
    moveDirection(696969);

    // Assert
    // Should pass if expected function is not called
}

void test_moveDirection_modifies_setpoint_to_good_values_if_FRONTSIDE() {
    // Fixture
    float expected1 = SPEED;
    float expected2 = NO_SPEED;
    float expected3 = HEIGHT_SP;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(FRONTSIDE);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}

void test_moveDirection_modifies_setpoint_to_good_values_if_LEFTSIDE() {
    // Fixture
    float expected1 = NO_SPEED;
    float expected2 = SPEED;
    float expected3 = HEIGHT_SP;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(LEFTSIDE);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}

void test_moveDirection_modifies_setpoint_to_good_values_if_BACKSIDE() {
    // Fixture
    float expected1 = -SPEED;
    float expected2 = NO_SPEED;
    float expected3 = HEIGHT_SP;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(BACKSIDE);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}

void test_moveDirection_modifies_setpoint_to_good_values_if_RIGHTSIDE() {
    // Fixture
    float expected1 = NO_SPEED;
    float expected2 = -SPEED;
    float expected3 = HEIGHT_SP;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(RIGHTSIDE);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}

void test_moveDirection_modifies_setpoint_to_good_values_if_DOWNSIDE() {
    // Fixture
    float expected1 = NO_SPEED;
    float expected2 = NO_SPEED;
    float expected3 = NO_SPEED;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(DOWNSIDE);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}

void test_moveDirection_modifies_setpoint_to_good_values_if_STOP() {
    // Fixture
    float expected1 = NO_SPEED;
    float expected2 = NO_SPEED;
    float expected3 = HEIGHT_SP;
    float expected4 = NO_ROTATION;
    float notExpected = 69.0;
    setpoint.position.z = notExpected;
    setpoint.attitudeRate.yaw = notExpected;
    setpoint.velocity.x = notExpected;
    setpoint.velocity.y = notExpected;

    // Test
    commanderSetSetpoint_Expect(&setpoint, PRIORITY_MOVE);
    moveDirection(STOP);
    
    // Assert
    TEST_ASSERT_EQUAL(expected1, setpoint.position.z);
    TEST_ASSERT_EQUAL(expected2, setpoint.attitudeRate.yaw);
    TEST_ASSERT_EQUAL(expected3, setpoint.velocity.x);
    TEST_ASSERT_EQUAL(expected4, setpoint.velocity.y);
}



#endif
