/** File under test ledController.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/


#include "../../app_api/src/ledSystem/ledController.h"
#include "unity.h"
// #include "mock_ledController.h"
#include "mock_ledseq.h"



void test_turnOnLed_calls_ledseqRegisterSequence() {
    // Fixture
    const int TIME_LED_ON = 1000;
    const int TIME_LED_OFF = 500;

    static ledseqStep_t ledSequenceStep[] = {
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_STOP},
    };

    static ledseqContext_t ledSequenceContext = {
        .sequence = ledSequenceStep,
        .led = LED_GREEN_R,
    };

    // Test
    ledseqRegisterSequence_Expect(&ledSequenceContext);
    ledseqRun_IgnoreAndReturn(&ledSequenceContext);
    ledseqRegisterSequence_IgnoreArg_context();
    turnOnLed();

    // Assert
    // Should pass if expected function is called
}

void test_turnOnLed_calls_ledseqRun() {
    // Fixture
    bool expected = true;
    const int TIME_LED_ON = 1000;
    const int TIME_LED_OFF = 500;

    static ledseqStep_t ledSequenceStep[] = {
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_STOP},
    };

    static ledseqContext_t ledSequenceContext = {
        .sequence = ledSequenceStep,
        .led = LED_GREEN_R,
    };

    // Test
    ledseqRegisterSequence_Ignore();
    ledseqRun_ExpectAndReturn(&ledSequenceContext, expected);
    ledseqRun_IgnoreArg_context();
    turnOnLed();

    // Assert
    // Should pass if expected function is called
}

void test_turnOnLed_returns_true_if_led_is_on() {
    // Fixture
    bool expected = true;
    const int TIME_LED_ON = 1000;
    const int TIME_LED_OFF = 500;

    static ledseqStep_t ledSequenceStep[] = {
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_WAITMS(TIME_LED_OFF)},
        {true, LEDSEQ_WAITMS(TIME_LED_ON)},
        {false, LEDSEQ_STOP},
    };

    static ledseqContext_t ledSequenceContext = {
        .sequence = ledSequenceStep,
        .led = LED_GREEN_R,
    };

    // Test
    ledseqRegisterSequence_Ignore();
    ledseqRun_IgnoreAndReturn(&ledSequenceContext);
    bool actual = turnOnLed();

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

