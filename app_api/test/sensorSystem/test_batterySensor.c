/** File under test batterySensor.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TEST_BATTERY_SENSOR
#define TEST_BATTERY_SENSOR


#include "../../app_api/src/sensorSystem/batterySensor.h"
#include "unity.h"
#include "mock_pm.h"


/**********************************************************
 * Tests getBatteryLevel()
 *********************************************************/

void test_getBatteryLevel_calls_pmGetBatteryVoltage() {
    // Fixture

    // Test
    pmGetBatteryVoltage_ExpectAndReturn(0);
    getBatteryLevel();
    
    // Assert
    // Should pass if expected function is called
}

void test_getBatteryLevel_returns_0_if_battery_level_too_low() {
    // Fixture
    uint8_t expected = 0;
    float voltage = 2.5;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_90_if_battery_level_too_high() {
    // Fixture
    uint8_t expected = 90;
    float voltage = 10.0;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_90_if_battery_level_is_90() {
    // Fixture
    uint8_t expected = 90;
    float voltage = 4.10;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_80_if_battery_level_is_between_75_and_80() {
    // Fixture
    uint8_t expected = 80;
    float voltage = 4.03;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_30_if_battery_level_is_30() {
    // Fixture
    uint8_t expected = 30;
    float voltage = 3.87;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_10_if_battery_level_is_between_0_and_10() {
    // Fixture
    uint8_t expected = 10;
    float voltage = 3.50;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}

void test_getBatteryLevel_returns_0_if_battery_level_is_0() {
    // Fixture
    uint8_t expected = 0;
    float voltage = 3.00;

    // Test
    pmGetBatteryVoltage_IgnoreAndReturn(voltage);
    uint8_t actual = getBatteryLevel();
    

    // Assert
    TEST_ASSERT_EQUAL(expected, actual);
}


#endif
