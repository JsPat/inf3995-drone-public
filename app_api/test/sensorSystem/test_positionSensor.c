/** File under test positionSensor.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TEST_POSITION_SENSOR
#define TEST_POSITION_SENSOR


#include "../../app_api/src/sensorSystem/positionSensor.h"
#include "unity.h"
// #include "mock_positionSensor.h"
#include "mock_log.h"
#include "mock_param.h"


/**********************************************************
 * Tests initializePositionSensor()
 *********************************************************/

void test_initializePositionSensor_modify_distanceSensorId() {
    // Fixture
    uint8_t expected = 69;
    distanceSensorId.idFront = 0;
    distanceSensorId.idLeft = 0;
    distanceSensorId.idBack = 0;
    distanceSensorId.idRight = 0;
    distanceSensorId.idUp = 0;
    distanceSensorId.idDown = 0;
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(expected);
    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idFront);
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idLeft);
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idBack);
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idRight);
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idUp);
    TEST_ASSERT_EQUAL(expected, distanceSensorId.idDown);
}

void test_initializePositionSensor_modify_positionSensorId() {
    // Fixture
    uint8_t expected = 69;
    positionSensorId.idX = 0;
    positionSensorId.idY = 0;
    positionSensorId.idZ = 0;
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(expected);
    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, positionSensorId.idX);
    TEST_ASSERT_EQUAL(expected, positionSensorId.idY);
    TEST_ASSERT_EQUAL(expected, positionSensorId.idZ);
}

void test_initializePositionSensor_modify_idPositioningDeck() {
    // Fixture
    paramVarId_t paramVarIdExpected = {
        .id = 69,
        .ptr = 69,
    };
    paramVarId_t paramVarIdInit = {
        .id = 0,
        .ptr = 0,
    };
    idPositioningDeck = paramVarIdInit;

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_IgnoreAndReturn(paramVarIdExpected);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(paramVarIdExpected.id, idPositioningDeck.id);
    TEST_ASSERT_EQUAL(paramVarIdExpected.ptr, idPositioningDeck.ptr);
}

void test_initializePositionSensor_modify_idMultiranger() {
    // Fixture
    paramVarId_t paramVarIdExpected = {
        .id = 69,
        .ptr = 69,
    };
    paramVarId_t paramVarIdInit = {
        .id = 0,
        .ptr = 0,
    };
    idMultiranger = paramVarIdInit;

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_IgnoreAndReturn(paramVarIdExpected);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(paramVarIdExpected.id, idMultiranger.id);
    TEST_ASSERT_EQUAL(paramVarIdExpected.ptr, idMultiranger.ptr);
}

void test_initializePositionSensor_modify_positioningInit() {
    // Fixture
    uint8_t expected = 69;
    positioningInit = 0;
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_IgnoreAndReturn(expected);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, positioningInit);
}

void test_initializePositionSensor_modify_multirangerInit() {
    // Fixture
    uint8_t expected = 69;
    multirangerInit = 0;
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_IgnoreAndReturn(expected);
    initializePositionSensor();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, multirangerInit);
}

void test_initializePositionSensor_calls_logGetVarId() {
    // Fixture
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_ExpectAndReturn("range", "front", 0);
    logGetVarId_ExpectAndReturn("range", "left", 0);
    logGetVarId_ExpectAndReturn("range", "back", 0);
    logGetVarId_ExpectAndReturn("range", "right", 0);
    logGetVarId_ExpectAndReturn("range", "up", 0);
    logGetVarId_ExpectAndReturn("range", "down", 0);

    logGetVarId_ExpectAndReturn("stateEstimate", "x", 0);
    logGetVarId_ExpectAndReturn("stateEstimate", "y", 0);
    logGetVarId_ExpectAndReturn("stateEstimate", "z", 0);

    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    // Should pass if expected function is called
}

void test_initializePositionSensor_calls_paramGetVarId() {
    // Fixture
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_ExpectAndReturn("deck", "bcFlow2", paramVarIdTest);
    paramGetVarId_ExpectAndReturn("deck", "bcMultiranger", paramVarIdTest);
    paramGetUint_IgnoreAndReturn(0);
    initializePositionSensor();
    
    // Assert
    // Should pass if expected function is called
}

void test_initializePositionSensor_calls_paramGetUint() {
    // Fixture
    paramVarId_t paramVarIdTest = {
        .id = 0,
        .ptr = 0,
    };

    // Test
    logGetVarId_IgnoreAndReturn(0);
    paramGetVarId_IgnoreAndReturn(paramVarIdTest);
    paramGetUint_ExpectAndReturn(idPositioningDeck, 0);
    paramGetUint_ExpectAndReturn(idMultiranger, 0);
    initializePositionSensor();
    
    // Assert
    // Should pass if expected function is called
}



/**********************************************************
 * Tests getCurrentDistancesDrone()
 *********************************************************/

void test_getCurrentDistancesDrone_calls_logGetUint() {
    // Fixture

    // Test
    logGetUint_ExpectAndReturn(distanceSensorId.idFront, 0);
    logGetUint_ExpectAndReturn(distanceSensorId.idLeft, 0);
    logGetUint_ExpectAndReturn(distanceSensorId.idBack, 0);
    logGetUint_ExpectAndReturn(distanceSensorId.idRight, 0);
    logGetUint_ExpectAndReturn(distanceSensorId.idUp, 0);
    // logGetUint_ExpectAndReturn(distanceSensorId.idDown, 0);
    getCurrentDistancesDrone();
    
    // Assert
    // Should pass if expected function is called
}

void test_getCurrentDistancesDrone_returns_distances() {
    // Fixture
    uint8_t expected = 69;

    // Test
    logGetUint_IgnoreAndReturn(expected);
    DistanceSensor distancesTest = getCurrentDistancesDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, distancesTest.front);
    TEST_ASSERT_EQUAL(expected, distancesTest.left);
    TEST_ASSERT_EQUAL(expected, distancesTest.back);
    TEST_ASSERT_EQUAL(expected, distancesTest.right);
    TEST_ASSERT_EQUAL(expected, distancesTest.up);
    // TEST_ASSERT_EQUAL(expected, distancesTest.down);
}

void test_getCurrentDistancesDrone_returns_distances_negative() {
    // Fixture
    uint8_t expected = -69;

    // Test
    logGetUint_IgnoreAndReturn(expected);
    DistanceSensor distancesTest = getCurrentDistancesDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, distancesTest.front);
    TEST_ASSERT_EQUAL(expected, distancesTest.left);
    TEST_ASSERT_EQUAL(expected, distancesTest.back);
    TEST_ASSERT_EQUAL(expected, distancesTest.right);
    TEST_ASSERT_EQUAL(expected, distancesTest.up);
    // TEST_ASSERT_EQUAL(expected, distancesTest.down);
}

void test_getCurrentDistancesDrone_returns_distances_0() {
    // Fixture
    uint8_t expected = 0;

    // Test
    logGetUint_IgnoreAndReturn(expected);
    DistanceSensor distancesTest = getCurrentDistancesDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, distancesTest.front);
    TEST_ASSERT_EQUAL(expected, distancesTest.left);
    TEST_ASSERT_EQUAL(expected, distancesTest.back);
    TEST_ASSERT_EQUAL(expected, distancesTest.right);
    TEST_ASSERT_EQUAL(expected, distancesTest.up);
    // TEST_ASSERT_EQUAL(expected, distancesTest.down);
}

void test_getCurrentDistancesDrone_returns_distances_MAX_UINT_8() {
    // Fixture
    uint8_t expected = 255;

    // Test
    logGetUint_IgnoreAndReturn(expected);
    DistanceSensor distancesTest = getCurrentDistancesDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected, distancesTest.front);
    TEST_ASSERT_EQUAL(expected, distancesTest.left);
    TEST_ASSERT_EQUAL(expected, distancesTest.back);
    TEST_ASSERT_EQUAL(expected, distancesTest.right);
    TEST_ASSERT_EQUAL(expected, distancesTest.up);
    // TEST_ASSERT_EQUAL(expected, distancesTest.down);
}

/**********************************************************
 * Tests getLastDistanceDrone()
 *********************************************************/

void test_getLastDistanceDrone_returns_lastDistanceSensor() {
    // Fixture
    DistanceSensor expected = {
        .front = 69,
        .left = 69,
        .back = 69,
        .right = 69,
        .up = 69,
    };
    lastDistanceSensor = expected;

    // Test
    DistanceSensor actual = getLastDistanceDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected.front, actual.front);
    TEST_ASSERT_EQUAL(expected.left, actual.left);
    TEST_ASSERT_EQUAL(expected.back, actual.back);
    TEST_ASSERT_EQUAL(expected.right, actual.right);
    TEST_ASSERT_EQUAL(expected.up, actual.up);
}


/**********************************************************
 * Tests getCurrentPositionDrone()
 *********************************************************/

void test_getCurrentPositionDrone_calls_logGetFloat() {
    // Fixture

    // Test
    logGetFloat_ExpectAndReturn(positionSensorId.idX, 0.0);
    logGetFloat_ExpectAndReturn(positionSensorId.idY, 0.0);
    logGetFloat_ExpectAndReturn(positionSensorId.idZ, 0.0);
    getCurrentPositionDrone();
    
    // Assert
    // Should pass if expected function is called
}

void test_getCurrentPositionDrone_modifies_lastPositionSensor() {
    // Fixture
    int16_t expected = 69;
    positionSensor.x = expected;
    positionSensor.y = expected;
    positionSensor.z = expected;
    lastPositionSensor.x = 0;
    lastPositionSensor.y = 0;
    lastPositionSensor.z = 0;

    // Test
    logGetFloat_IgnoreAndReturn(0.0);
    getCurrentPositionDrone();

    int16_t actualX = lastPositionSensor.x;
    int16_t actualY = lastPositionSensor.y;
    int16_t actualZ = lastPositionSensor.z;

    // Assert
    TEST_ASSERT_EQUAL(expected, actualX);
    TEST_ASSERT_EQUAL(expected, actualY);
    TEST_ASSERT_EQUAL(expected, actualZ);
}

void test_getCurrentPositionDrone_returns_position_in_mm() {
    // Fixture
    float expectedInM = 0.69f;
    int16_t expectedInMM = (int16_t)(expectedInM  * 1000.0f);

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.x);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.y);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.z);
}

void test_getCurrentPositionDrone_x_y_position_can_be_negative() {
    // Fixture
    float expectedInM = -0.69f;
    int16_t expectedInMM = (int16_t)(expectedInM  * 1000.0f);

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.x);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.y);
}

void test_getCurrentPositionDrone_z_position_cant_be_negative() {
    // Fixture
    float expectedInM = -0.69f;
    int16_t expectedInMM = 0;

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.z);
}

void test_getCurrentPositionDrone_x_y_z_position_0() {
    // Fixture
    float expectedInM = 0;
    int16_t expectedInMM = (int16_t)(expectedInM  * 1000.0f);

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.x);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.y);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.z);
}

void test_getCurrentPositionDrone_x_y_z_position_MAX_INT_16() {
    // Fixture
    float expectedInM = 32.767f;
    int16_t expectedInMM = (int16_t)(expectedInM  * 1000.0f);

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.x);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.y);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.z);
}

void test_getCurrentPositionDrone_x_y_position_MIN_INT_16() {
    // Fixture
    float expectedInM = -32.768f;
    int16_t expectedInMM = (int16_t)(expectedInM  * 1000.0f);

    // Test
    logGetFloat_IgnoreAndReturn(expectedInM);
    Coordinates positionTest = getCurrentPositionDrone();
    getCurrentPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.x);
    TEST_ASSERT_EQUAL(expectedInMM, positionTest.y);
}

/**********************************************************
 * Tests getLastPositionDrone()
 *********************************************************/

void test_getLastPositionDrone_returns_lastDistanceSensor() {
    // Fixture
    Coordinates expected = {
        .x = 69,
        .y = 69,
        .z = 69,
    };
    lastPositionSensor = expected;

    // Test
    Coordinates actual = getLastPositionDrone();
    
    // Assert
    TEST_ASSERT_EQUAL(expected.x, actual.x);
    TEST_ASSERT_EQUAL(expected.y, actual.y);
    TEST_ASSERT_EQUAL(expected.z, actual.z);
}


#endif

