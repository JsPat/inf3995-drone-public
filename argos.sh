#!/bin/bash

containerId=$(docker ps -qf "name=argos-example")
if [ "$containerId" = "" ]; then
    echo "Go."
else
    echo "Container still."
    docker stop "$containerId"
fi

for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   

    case "$KEY" in
            X11)              X11=${VALUE} ;;
            ARGOS_FILE)    ARGOS_FILE=${VALUE} ;;
            test)    	TEST=1 ;;     
            *)   
    esac    


done

echo "X11 = $X11"
echo "ARGOS_FILE = $ARGOS_FILE"

if [ "$ARGOS_FILE" = "" ]; then
    echo "Go."
    exit 1
fi


x11docker $X11 --hostnet --user=RETAIN -- "-p 5566:5566" --privileged  argos-example &

sleep 5
echo 'GET'
containerId=$(docker ps -qf "name=argos-example")
echo $containerId

# Copy argos simulation files on docker
docker cp argos/examples/ $containerId:/root/
# Copy crazyflie common files on docker for argos to use
docker cp app_api/src/common $containerId:/root/examples/controllers

if [ "$TEST" = "1" ]; then
    # Execute argos tests on docker
    docker exec -it $containerId bash -c "cd /root/examples/controllers; mkdir /root/examples/build/controllers/test; ctest --build-and-test /root/examples/controllers/test /root/examples/build/controllers/test --build-generator \"Unix Makefiles\" --test-command ./tests_argos; "
   
# /root/examples/build/controllers/test
else
    # Execute argos simulation on docker
    docker exec -it $containerId bash -c "cd /root/examples/build; make; cd /root/examples; argos3 -c experiments/$ARGOS_FILE"
fi



