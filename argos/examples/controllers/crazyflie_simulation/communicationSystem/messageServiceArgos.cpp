/**
 * This file contains the message service for ARGoS
 */

#include "messageServiceArgos.h"


MessageServiceArgos::MessageServiceArgos() {
    connectionStatus = false;
    currentMessageId = 0;
}


void MessageServiceArgos::initSocket() {
   struct sockaddr_in serv_addr;

   if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      printf("\n Socket creation error \n");
      return;
   }

   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port = htons(PORT);

   // Convert IPv4 and IPv6 addresses from text to binary form
   if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
      printf("\nInvalid address/ Address not supported \n");
      return;
   }

   if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
      printf("\nConnection Failed \n");
      return;
   }

   connectionStatus = true;
   std::thread thread_1(&MessageServiceArgos::receiveMessageArgos, this);
   thread_1.detach();
}


void MessageServiceArgos::sendMessageArgos(uint8_t batteryLevel,
        float rangingDeck[MAX_SENSORS], Coordinates coords, Coordinates coordsPrevious) {
    if (!connectionStatus) {
        initSocket();
    }
    if (waitMessageToSendCounter >= STEPS_UNTIL_SEND_MESSAGE) {
            send(sock, generateMessageArgos(batteryLevel,
                rangingDeck, coords, coordsPrevious),
                MESSAGE_LENGTH, 0);
            waitMessageToSendCounter = 0;
    }
    waitMessageToSendCounter++;
}


uint8_t* MessageServiceArgos::generateMessageArgos(uint8_t batteryLevel,
        float rangingDeck[MAX_SENSORS], Coordinates coords, Coordinates coordsPrevious) {
    currentMessageToSend.header.messageId = currentMessageId++;
    currentMessageToSend.header.sourceId = droneSourceId;
    currentMessageToSend.header.destinationId = ID_BASE_STATION;
    currentMessageToSend.header.messageType = INFO_MAPPING;

    currentMessageToSend.body.droneState = *state;
    currentMessageToSend.body.batteryLevel = batteryLevel;
    
    currentMessageToSend.body.distanceFront = (uint16_t)rangingDeck[FRONTSIDE];
    currentMessageToSend.body.distanceLeft = (uint16_t)rangingDeck[LEFTSIDE];
    currentMessageToSend.body.distanceBack = (uint16_t)rangingDeck[BACKSIDE];
    currentMessageToSend.body.distanceRight = (uint16_t)rangingDeck[RIGHTSIDE];

    currentMessageToSend.body.dronePositionX = coords.x;
    currentMessageToSend.body.dronePositionY = coords.y;
    currentMessageToSend.body.dronePositionZ = coords.z;

    currentMessageToSend.body.droneSpeedX = getSpeed(coords.x, coordsPrevious.x);
    currentMessageToSend.body.droneSpeedY = getSpeed(coords.y, coordsPrevious.y);
    currentMessageToSend.body.droneSpeedZ = getSpeed(coords.z, coordsPrevious.z);

    return generateBufferFromMessage();
}


uint8_t* MessageServiceArgos::generateBufferFromMessage() {
    // This implementation is ugly but necessary, because members of struct are of different
    // sizes and we want to fit every member in a communication packet to send to server
    
    uint8_t positionInBuffer = 0;
    // Header
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.header.messageId & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.header.messageId >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)currentMessageToSend.header.sourceId;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)currentMessageToSend.header.destinationId;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)currentMessageToSend.header.messageType;
    // Body
    bufferMessageToSend[positionInBuffer++] = (uint8_t)currentMessageToSend.body.droneState;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)currentMessageToSend.body.batteryLevel;
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceFront & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceFront >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceLeft & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceLeft >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceBack & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceBack >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceRight & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.distanceRight >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedX & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedX >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedY & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedY >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedZ & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.droneSpeedZ >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionX & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionX >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionY & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionY >> 8);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionZ & 0xFF);
    bufferMessageToSend[positionInBuffer++] = (uint8_t)(currentMessageToSend.body.dronePositionZ >> 8);

    return bufferMessageToSend;
}


void MessageServiceArgos::receiveMessageArgos() {
   while (true) {
        int valread = read(sock, bufferMessageReceived, MESSAGE_LENGTH);
        command = readCommandFromMessage();

        switch (command) {
        case COMMAND_STANDBY:
            *state = STATE_STANBY;
            break;
        
        case COMMAND_TAKEOFF:
            *state = STATE_TAKEOFF;
            break;
        
        case COMMAND_LAND:
            *state = STATE_LANDING;
            break;
        
        case COMMAND_RETURN_TO_BASE:
            *state = STATE_RETURN_TO_BASE;
            break;
        
        default:
            break;
        }
    }
}


enum CommandType MessageServiceArgos::readCommandFromMessage() {
    command = COMMAND_NO_COMMAND;
    currentMessageReceived.header.messageId =
        (bufferMessageReceived[POSITION_MESSAGE_ID_0] << 8 | bufferMessageReceived[POSITION_MESSAGE_ID_1]);
    currentMessageReceived.header.sourceId = bufferMessageReceived[POSITION_MESSAGE_SOURCE_ID];
    currentMessageReceived.header.destinationId = bufferMessageReceived[POSITION_MESSAGE_DESTINATION_ID];
    currentMessageReceived.header.messageType = bufferMessageReceived[POSITION_MESSAGE_TYPE];

    // Message not for this drone so ignore it
    if (currentMessageReceived.header.destinationId != droneSourceId && currentMessageReceived.header.destinationId != BROADCAST) {
        return COMMAND_NO_COMMAND;
    }

    switch (currentMessageReceived.header.messageType)
    {
    case (uint8_t)INFO_MAPPING:
        // Ignore, because not used in this implementation for ARGoS
        break;
    
    case (uint8_t)COMMAND:
        for (uint8_t bufferPosition = MESSAGE_HEADER_LENGTH; bufferPosition < MESSAGE_BODY_LENGTH; ++bufferPosition) {
            currentMessageReceived.body[bufferPosition - MESSAGE_HEADER_LENGTH] = bufferMessageReceived[bufferPosition];
        }
        // Only read first byte, because if the message is a command, only the first byte is useful
        if ((uint8_t)currentMessageReceived.body[0] < COMMAND_NO_COMMAND
        || (uint8_t)currentMessageReceived.body[0] >= NUMBER_OF_COMMANDS) {
            // Invalid command so return a COMMAND_NO_COMMAND
            return COMMAND_NO_COMMAND;
        }
        command = (enum CommandType)currentMessageReceived.body[0];
        break;
    
    default:
        break;
    }
    return command;
}


int16_t MessageServiceArgos::getSpeed(int16_t coordinate, int16_t coordinatePrevious) {
   // Ignore if first step
   if (coordinatePrevious == 0) {
      return 0;
   }
   return (int16_t)((coordinate - coordinatePrevious) / SECONDS_PER_STEP);
}

