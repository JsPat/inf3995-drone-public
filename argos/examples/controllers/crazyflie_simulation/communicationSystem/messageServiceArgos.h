/**
 * This file contains the message service for ARGoS
 */

#ifndef MESSAGE_SERVICE_ARGOS_H
#define MESSAGE_SERVICE_ARGOS_H

// #include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream> // std::cout
#include <thread>   // std::thread
#include <cstdlib>
// Client side C/C++ program to demonstrate Socket programming
#include <arpa/inet.h>
#include "../../common/message.h"
#include "../../common/state.h"
#include "../../common/coordinates.h"
#include "../../common/controlConstants.h"

#define PORT 5566
#define STEPS_UNTIL_SEND_MESSAGE 9
#define TICKS_PER_SECOND 10.0
#define SECONDS_PER_STEP (1.0 / TICKS_PER_SECOND)
#ifndef M_TO_MM
#define M_TO_MM 1000.0
#define BROADCAST 255
#endif

class MessageServiceArgos
{
    public:
        uint8_t bufferMessageReceived[MESSAGE_LENGTH];
        uint8_t bufferMessageToSend[MESSAGE_LENGTH];
        MessageCommand currentMessageReceived;
        Message currentMessageToSend;

        uint16_t currentMessageId;
        uint8_t droneSourceId;
        enum CommandType command;
        State *state;
        

        /**
         * Constructor of MessageServiceArgos
         */
        MessageServiceArgos();

        /**
         * Initialize connection between drone (Argos) and station
         */
        void initSocket();

        /**
         * Sends a message from drone to station
         */
        void sendMessageArgos(uint8_t batteryLevel,
            float rangingDeck[4], Coordinates coords, Coordinates coordsPrevious);

        /**
         * Generates a message to send 
         */
        uint8_t* generateMessageArgos(uint8_t batteryLevel,
            float rangingDeck[4], Coordinates coords, Coordinates coordsPrevious);

        /**
         * Generates a buffer to send from a message 
         */
        uint8_t* generateBufferFromMessage();

        /**
         * Received a message and dispatch the command received
         */
        void receiveMessageArgos();

        /**
         * Read the command received from the message from station
         */
        enum CommandType readCommandFromMessage();

        /**
        * Get one dimension of the current speed of the drone to send in message
        */
        int16_t getSpeed(int16_t coordinate, int16_t coordinatePrevious);
    
    private:
        bool connectionStatus;
        uint8_t waitMessageToSendCounter;
        int sock;
};



#endif
