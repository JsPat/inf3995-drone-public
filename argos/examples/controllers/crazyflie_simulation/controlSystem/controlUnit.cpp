/* Include the controller definition */
#include "controlUnit.h"

ControlUnit::ControlUnit() : 
set(false),
state(STATE_STANBY),
newState(true),
step(0)
{
   mission = new Mission();
}

ControlUnit::~ControlUnit(){}

void ControlUnit::init(Coordinates coords, float rangingDeck[], uint8_t battery)
{
   pos = coords;
   batteryLevel = battery;
   if (!set) {
      basePos = coords;
      set = true;
   }
   if (batteryLevel <= BATTERY_VERY_LOW) {
      state = STATE_LANDING;
   } else if (batteryLevel <= BATTERY_LOW) {
      state = STATE_RETURN_TO_BASE;
   }
   mission->init(coords, rangingDeck);
}

void ControlUnit::dispatchState()
{
   switch (state){
      case STATE_STANBY:
         break;
      case STATE_TAKEOFF: 
         if (batteryLevel >= BATTERY_LOW) {
            if(newState)
               takeOff();
            if(nearDestination()) {
               newState = true;
               state = STATE_IN_MISSION;
            }
         }
         break;
      case STATE_IN_MISSION:
         mission->executeMission(); 
         break;
      case STATE_LANDING:
         if(newState)
            land();
         if(nearDestination()){
            newState = true;
            state = STATE_STANBY;
         }
         break;
      case STATE_NEED_RECHARGE:
         break;
      case STATE_CHARGING:
         break;
      case STATE_UPDATING:
         break;
      case STATE_CRASHED:
         break;
      case STATE_RETURN_TO_BASE:
         if (mission->returnToBase()){
            state = STATE_LANDING;
         }
         break;
   }
}

/****************************************/
/****************************************/

void ControlUnit::takeOff()
{
   mission->destination.z = TAKEOFF_HEIGHT;
   newState = false;
}

/****************************************/
/****************************************/

void ControlUnit::land()
{
   mission->destination.z = GROUND;
   newState = false;
}

/****************************************/
/****************************************/

bool ControlUnit::nearDestination()
{
   float num = sqrt(pow((mission->destination.x - pos.x), EXPONENT_TWO) 
                  + pow((mission->destination.y - pos.y), EXPONENT_TWO) 
                  + pow((mission->destination.z - pos.z), EXPONENT_TWO));
   return (num < GAP_DESTINATION);
}