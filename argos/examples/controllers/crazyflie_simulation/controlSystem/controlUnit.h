#ifndef CONTROL_UNIT_H
#define CONTROL_UNIT_H

/*
 * Include some necessary headers.
 */

#include <cstdlib>
#include <cmath>
#include <stdlib.h>

#include "../../common/coordinates.h"
#include "../../common/state.h"
#include "mission.h"

#define TAKEOFF_HEIGHT 1000
#define GROUND 0
#define GAP_DESTINATION 500
#define GAP_GROUND 10
#define EXPONENT_TWO 2

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

class ControlUnit
{

public:
    /**
    * Default constructor of ControlUnit
    */
    ControlUnit();

    /**
    * Default delete of ControlUnit
    */
    ~ControlUnit();

    /**
    * Initialize all values that need to be initialize before each step
    */
    void init(Coordinates coords, float rangingDeck[], uint8_t battery);

    /**
    * Execute what is needed depending on drone state
    */
    void dispatchState();

    /**
    * Make the drone take off
    */
    void takeOff();

    /**
    * Make the drone land
    */
    void land();

    /**
    * Return true if the drone is near where it's supposed to be
    */
    bool nearDestination();
    
    Mission *mission;
    State state;

private:
    Coordinates basePos;
    Coordinates pos;
    bool set;
    bool newState;
    int step;
    uint8_t batteryLevel;
};

#endif