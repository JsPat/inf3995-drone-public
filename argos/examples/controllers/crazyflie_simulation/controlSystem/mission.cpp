/* Include the mission definition */
#include "mission.h"



Mission::Mission() :
    set(false),
    side(FRONTSIDE),
    speed(NORMAL_SPEED),
    sideRTB(FRONTSIDE),
    startRTB(true),
    turningLeft(0),
    changeLeftSide(true)
{}

Mission::~Mission()
{}

void Mission::init(Coordinates coords, float sensors[])
{
    position = coords;
    for(int i = 0; i < MAX_SENSORS; i++)
        rangingDeck[i] = sensors[i];
    
    if(!set){
        destination = coords;
        basePosition = coords;
        set = true;
    }
    basePosition.x = BASE_STATION_POSITION_X;
    basePosition.y = BASE_STATION_POSITION_Y;
}

void Mission::executeMission()
{
    moveDirection(side);

    if(side == STOP){
        wallDetection(previousSideForStop);
    } else {
        wallDetection(side);
    }
}

void Mission::move(float x, float y, float z)
{
    destination.x += x;
    destination.y += y;
    destination.z += z;
}

void Mission::moveLeft()
{
    move(NO_MOUVEMENT, speed, NO_MOUVEMENT);
}

void Mission::moveRight()
{
    move(NO_MOUVEMENT, -speed, NO_MOUVEMENT);
}

void Mission::moveFront()
{
    move(speed, NO_MOUVEMENT, NO_MOUVEMENT);
}

void Mission::moveBack()
{
    move(-speed, NO_MOUVEMENT, NO_MOUVEMENT);

}

void Mission::moveDirection(int side)
{
    switch(side){
        case FRONTSIDE:
            moveFront();
            break;
        case LEFTSIDE:
            moveLeft();
            break;
        case BACKSIDE:
            moveBack();
            break;
        case RIGHTSIDE:
            moveRight();
            break;
        case STOP:
            stop();
            break;
    }   
}

void Mission::stop()
{
    destination.x = position.x;
    destination.y = position.y;
    destination.z = position.z;
}

void Mission::wallDetection(int sensorSide)
{
    bool sideAvailable [MAX_SENSORS];
    if(isWallClose(sensorSide, RADIUS)){
        //Stop the drone
        stop();
        int nbSideAvailable = 0;
        for(int i = 0; i < MAX_SENSORS; i++){
            sideAvailable[i] = !(isWallClose(i, MINI_GAP));
            if(sideAvailable[i]){
                nbSideAvailable++;
                side = i;
            }
        }
        if(nbSideAvailable == NO_SIDE_AVAILABLE){
            previousSideForStop = sensorSide;
            side = STOP;
        }
        else if (nbSideAvailable != ONE_SIDE_AVAILABLE){
            srand(time(NULL));
            while(true){
                side = rand() % MAX_SENSORS;
                if(sideAvailable[side])
                    return;
            }
        }
        return;
    }
    for(int i = 0; i < MAX_SENSORS; i++){
        if(isWallClose(i, MINI_GAP)){
            speed = LOW_SPEED;
            if(i <= 1){
                if(!isWallClose(i + OTHER_SIDE_SENSOR, MINI_GAP)){
                    moveDirection(i + OTHER_SIDE_SENSOR);
                    return;
                }
            } else {
                if(!isWallClose(i - OTHER_SIDE_SENSOR, MINI_GAP)){
                    moveDirection(i - OTHER_SIDE_SENSOR);
                    return;
                }
            }
        }
        else {
            speed = NORMAL_SPEED;
        }
    }
}

bool Mission::isWallClose(int side, float lenght)
{
    return rangingDeck[side] < lenght && rangingDeck[side] >= 0;
}

bool Mission::returnToBase()
{
    speed = LOW_SPEED;
    logicRTB(sideRTB);
    return (Abs(basePosition.x - position.x) < MAX_DISTANCE_FROM_BASE 
            && Abs(basePosition.y - position.y) < MAX_DISTANCE_FROM_BASE);
}

void Mission::logicRTB(int side)
{
    if(changeLeftSide){
        moveDirection(side);
        if(isWallClose(changeSide(side, TURN_LEFT), RADIUS)){
            changeLeftSide = false;
        }
    }
    else if(!isWallClose(changeSide(side, TURN_LEFT), RADIUS) && turningLeft < MAX_SENSORS){
        sideRTB = changeSide(side, TURN_LEFT);
        changeLeftSide = true;
        turningLeft++;
        return;
    }
    if(isWallClose(sideRTB, RADIUS)){
        sideRTB = changeSide(side, TURN_RIGHT);
        if(turningLeft > NO_TURN_LEFT){
            turningLeft--;
        } else if(turningLeft > MAX_SENSORS){
            turningLeft = NO_TURN_LEFT;
        }
    } else {
        if(isWallClose(changeSide(side, TURN_LEFT), MINI_GAP)){
            moveDirection(changeSide(side, TURN_RIGHT));
        }
        moveDirection(side);
    }
}

int Mission::changeSide(int side, int nbChange)
{
    if (nbChange >= 0) {
        for(int i = 1; i <= nbChange; ++i){
            side++;
            if(side > RIGHTSIDE){
                side = FRONTSIDE;
            }
        }
    }
    return side;
}