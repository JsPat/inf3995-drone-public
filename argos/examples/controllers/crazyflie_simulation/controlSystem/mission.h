#ifndef MISSION_H
#define MISSION_H

/*
 * Include some necessary headers.
 */

#include <cstdlib>
#include <argos3/core/utility/math/vector3.h>
#include <cmath>

#include "../../common/coordinates.h"
#include "../../common/state.h"
#include "../../common/controlConstants.h"

#define NO_MOUVEMENT 0
#define LOW_SPEED 10
#define NORMAL_SPEED 20
#define MINI_GAP 250
#define RADIUS 500
#define BASE_STATION_POSITION_X -4000
#define BASE_STATION_POSITION_Y -4000
#define OTHER_SIDE_SENSOR 2



/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

class Mission
{
public:
    /**
    * Default constructor of Mission
    */
    Mission();

    /**
    * Default delete of Mission
    */
    ~Mission();

    /**
    * Initialize all values that need to be initialize before each step
    */
    void init(Coordinates coords, float sensors[]);

    /**
    * Execute a step of the mission
    */
    void executeMission();

    /**
    * Move the drone with specific coordinates values
    */
    void move(float x, float y, float z);

    /**
    * Move the drone to the left
    */
    void moveLeft();

    /**
    * Move the drone to the right
    */
    void moveRight();

    /**
    * Move the drone to the front
    */
    void moveFront();

    /**
    * Move the drone to the back
    */
    void moveBack();

    /**
    * Move the drone to a specific side
    */
    void moveDirection(int side);

    /**
    * Stop the drone
    */
    void stop();

    /**
    * Detect if there is a wall at a specific side and avoid the wall
    */
    void wallDetection(int sensorSide);

    /**
    * Return true if a wall is close to this side
    */
    bool isWallClose(int side, float lenght);

    /**
    * Return the drone to the base
    */
    bool returnToBase();

    /**
    * The logic to return the drone to the base
    */
    void logicRTB(int side);

    /**
    * Change the side of the movement for the drone
    */
    int changeSide(int side, int nbChange);

    Coordinates destination;
    Coordinates position;
    Coordinates basePosition;
private:
    float rangingDeck [MAX_SENSORS];
    bool set;
    int side;
    int sideRTB;
    int previousSideForStop;
    float speed;
    bool startRTB;
    bool changeLeft;
    bool changeFront;
    bool changeBack;
    bool changeRight;
    int turningLeft;
    bool changeLeftSide;
;};

#endif