/**
 * Inspired by https://github.com/MISTLab/argos3-examples/blob/inf3995/controllers/crazyflie_sensing/crazyflie_sensing.cpp
 */

/* Include the controller definition */
#include "crazyflieSimulation.h"


CCrazyflieSensing::CCrazyflieSensing() : m_pcDistance(NULL), m_pcPropellers(NULL), m_pcRNG(NULL),
   m_pcRABA(NULL), m_pcRABS(NULL), m_pcPos(NULL), m_pcBattery(NULL), m_uiCurrentStep(0) {
   controlUnit = new ControlUnit();
   messageService = new MessageServiceArgos();
   MessageHeader header;
   MessageBody body;
   messageService->currentMessageToSend = {
      .header = header,
      .body = body,
   };
   messageService->state = &(controlUnit->state);
}

void CCrazyflieSensing::Init(TConfigurationNode &t_node) {
   try {
      /*
       * Initialize sensors/actuators
       */
      m_pcDistance = GetSensor<CCI_CrazyflieDistanceScannerSensor>("crazyflie_distance_scanner");
      m_pcPropellers = GetActuator<CCI_QuadRotorPositionActuator>("quadrotor_position");
      /* Get pointers to devices */
      m_pcRABA = GetActuator<CCI_RangeAndBearingActuator>("range_and_bearing");
      m_pcRABS = GetSensor<CCI_RangeAndBearingSensor>("range_and_bearing");
      try {
         m_pcPos = GetSensor<CCI_PositioningSensor>("positioning");
      } catch (CARGoSException &ex) {}
      try {
         m_pcBattery = GetSensor<CCI_BatterySensor>("battery");
      } catch (CARGoSException &ex){}
   } catch (CARGoSException &ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the crazyflie sensing controller for robot \"" << GetId() << "\"", ex);
   }

   /* Create a random number generator. We use the 'argos' category so
      that creation, reset, seeding and cleanup are managed by ARGoS. */
   m_pcRNG = CRandom::CreateRNG("argos");
   m_uiCurrentStep = 0;
   m_uiflying = false;
   seed = m_pcRNG->GetSeed();
   messageService->droneSourceId = (uint8_t)(seed);
   Reset();
}

void CCrazyflieSensing::ControlStep() {
   //Action to make for the previous step 
   m_pcPropellers->SetAbsolutePosition(CVector3((float)controlUnit->mission->destination.x / M_TO_MM, 
                                                (float)controlUnit->mission->destination.y / M_TO_MM, 
                                                (float)controlUnit->mission->destination.z / M_TO_MM));
   //Update sensors
   getPositionSensors();
   getDistanceSensors();
   batteryLevel = getBattery();

   messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);

   controlUnit->init(coords, rangingDeck, batteryLevel);
   controlUnit->dispatchState();
}

void CCrazyflieSensing::getPositionSensors() {
   coordsPrevious.x = coords.x;
   coordsPrevious.y = coords.y;
   coordsPrevious.z = coords.z;

   coords.x = m_pcPos->GetReading().Position.GetX() * M_TO_MM;
   coords.y = m_pcPos->GetReading().Position.GetY() * M_TO_MM;
   coords.z = m_pcPos->GetReading().Position.GetZ() * M_TO_MM;
}

void CCrazyflieSensing::getDistanceSensors() {
   const CCI_CrazyflieDistanceScannerSensor::TReadingsMap sDistRead = m_pcDistance->GetReadingsMap();
   auto tab = sDistRead.begin();
   rangingDeck[FRONTSIDE] = (tab++)->second * CM_TO_MM;
   rangingDeck[LEFTSIDE] = (tab++)->second * CM_TO_MM;
   rangingDeck[BACKSIDE] = (tab++)->second * CM_TO_MM;
   rangingDeck[RIGHTSIDE] = (tab++)->second * CM_TO_MM;
}

uint8_t CCrazyflieSensing::getBattery() {
   const CCI_BatterySensor::SReading &sBatRead = m_pcBattery->GetReading();
   return (uint8_t)(double)(sBatRead.AvailableCharge * MAX_BATTERY_LEVEL_ARGOS);
}

void CCrazyflieSensing::Reset() {}

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the XML configuration file to refer to
 * this controller.
 * When ARGoS reads that string in the XML file, it knows which controller
 * class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CCrazyflieSensing, "crazyflie_sensing_controller")
