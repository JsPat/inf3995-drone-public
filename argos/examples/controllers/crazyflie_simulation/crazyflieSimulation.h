/**
 * A crazyflie drones sensing.
 * 
 * Inspired from the crazyflie_simulation file from:
 * AUTHORS: Carlo Pinciroli <cpinciro@ulb.ac.be>
 *          Pierre-Yves Lajoie <lajoie.py@gmail.com>
 *
 * This controller is meant to be used with the XML file:
 *    experiments/crazyflie_simulation.argos
 */

#ifndef CRAZYFLIE_SENSING_H
#define CRAZYFLIE_SENSING_H


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream> // std::cout
#include <thread>   // std::thread
#include <cstdlib>

/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the crazyflie distance sensor */
#include <argos3/plugins/robots/crazyflie/control_interface/ci_crazyflie_distance_scanner_sensor.h>
/* Definition of the crazyflie position actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_quadrotor_position_actuator.h>
/* Definition of the crazyflie position sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>
/* Definition of the crazyflie range and bearing actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
/* Definition of the crazyflie range and bearing sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>
/* Definition of the crazyflie battery sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_battery_sensor.h>
/* Definitions for random number generation */
#include <argos3/core/utility/math/rng.h>

#include <argos3/core/utility/networking/tcp_socket.h>

#include "argos3/core/simulator/space/space_multi_thread_balance_length.h"

/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
/* Logging */
#include <argos3/core/utility/logging/argos_log.h>

#include "argos3/core/simulator/space/space_multi_thread_balance_length.h"


#include "controlSystem/controlUnit.h"
#include "controlSystem/mission.h"
#include "../common/coordinates.h"
#include "../common/state.h"
#include "communicationSystem/messageServiceArgos.h"


#define MAX 80
#define CM_TO_MM 10.0
#ifndef M_TO_MM
#define M_TO_MM 1000.0
#endif
#define DECIMAL_TO_POURCENTAGE 100.0

#define MAX_BATTERY_LEVEL_ARGOS 100.0

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CCrazyflieSensing : public CCI_Controller
{

public:
   /* Class constructor. */
   CCrazyflieSensing();
   /* Class destructor. */
   virtual ~CCrazyflieSensing() {}

   /*
    * This function initializes the controller.
    * The 't_node' variable points to the <parameters> section in the XML
    * file in the <controllers><footbot_foraging_controller> section.
    */
   virtual void Init(TConfigurationNode &t_node);

   /*
    * This function is called once every time step.
    * The length of the time step is set in the XML file.
    */
   virtual void ControlStep();

   /*
    * This function resets the controller to its state right after the
    * Init().
    * It is called when you press the reset button in the GUI.
    */
   virtual void Reset();

   /*
    * Called to cleanup what done by Init() when the experiment finishes.
    * In this example controller there is no need for clean anything up,
    * so the function could have been omitted. It's here just for
    * completeness.
    */
   virtual void Destroy() {}

   /**
    * Get coordinates of the current position of the drone
    */
   void getPositionSensors();

   /**
    * Get the rangingDeck values of the current distances measured around the drone
    */
   void getDistanceSensors();

   /**
    * get the battery level of the drone
    */
   uint8_t getBattery();

private:
   /* Pointer to the crazyflie distance sensor */
   CCI_CrazyflieDistanceScannerSensor *m_pcDistance;

   /* Pointer to the position actuator */
   CCI_QuadRotorPositionActuator *m_pcPropellers;

   /* Pointer to the range and bearing actuator */
   CCI_RangeAndBearingActuator *m_pcRABA;

   /* Pointer to the range and bearing sensor */
   CCI_RangeAndBearingSensor *m_pcRABS;

   /* Pointer to the positioning sensor */
   CCI_PositioningSensor *m_pcPos;

   /* Pointer to the battery sensor */
   CCI_BatterySensor *m_pcBattery;

   /* The random number generator */
   CRandom::CRNG *m_pcRNG;

   /* Current step */
   uint m_uiCurrentStep;

   /* Current state */
   bool m_uiflying;

   /* TCP socket */
   CTCPSocket s;

   int sock;

   UInt32 seed;

   float rangingDeck [MAX_SENSORS];
   Coordinates coords;
   Coordinates coordsPrevious;
   uint8_t batteryLevel;
   ControlUnit *controlUnit;
   MessageServiceArgos* messageService;

};

#endif
