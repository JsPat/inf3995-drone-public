cmake_minimum_required(VERSION 3.10)

find_package(Threads)

set(
	TEST_FILES
	assertsTestsArgos.h
	test_messageServiceArgos.cpp
	test_controlUnit.cpp
	test_mission.cpp
	test_crazyflieSimulation.cpp
	test_main.cpp
)

foreach(file ${TEST_FILES})
	add_custom_command(
		OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${file}"
		COMMAND cmake -E copy "${CMAKE_CURRENT_SOURCE_DIR}/${file}" "${CMAKE_CURRENT_BINARY_DIR}/${file}" DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${file}"
	)
	list(APPEND file_dest "${CMAKE_CURRENT_BINARY_DIR}/${file}")
endforeach(file)

add_executable(tests_argos /root/examples/controllers/test/test_main.cpp)
set_target_properties (tests_argos PROPERTIES LINKER_LANGUAGE CXX )
target_link_libraries(tests_argos ${CMAKE_THREAD_LIBS_INIT})
add_custom_target(test_main.cpp ALL DEPENDS ${TEST_FILES})
add_test(NAME tests_argos COMMAND ./tests_argos)

