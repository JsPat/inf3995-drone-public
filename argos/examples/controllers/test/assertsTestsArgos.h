/**
 * Asserts necessary for argos tests
 */

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_RESET   "\x1b[0m"
#define PRINT_SUCCESS() printf(COLOR_GREEN "SUCCESS" COLOR_RESET "\n")
#define PRINT_FAIL() printf(COLOR_RED "FAIL" COLOR_RESET "\n")
#define ASSERT_EQUAL(x,y) (x==y?PRINT_SUCCESS():PRINT_FAIL())
#define ASSERT_NOT_EQUAL(x,y) (x!=y?PRINT_SUCCESS():PRINT_FAIL())



