/** File under test controlUnit.cpp
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TESTS_CONTROL_UNIT_ARGOS
#define TESTS_CONTROL_UNIT_ARGOS

#include <cassert> 
#include "stdio.h"

#include "assertsTestsArgos.h"
#include "/root/examples/controllers/crazyflie_simulation/controlSystem/controlUnit.h"
#include "/root/examples/controllers/crazyflie_simulation/controlSystem/controlUnit.cpp"
#include "/root/examples/controllers/common/state.h"


ControlUnit* controlUnit;

/**
 * Setup function to execute before each tests
 */
void setupControlUnit() {
    controlUnit = new ControlUnit();
}



/**********************************************************
 * Tests ControlUnit()
 *********************************************************/

void test_ControlUnit_creates_new_ControlUnit() {
    setupControlUnit();
    // Fixture
    State notExpected = STATE_CRASHED;
    controlUnit->state = notExpected;

    // Test
    controlUnit = new ControlUnit();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_ControlUnit_creates_new_ControlUnit\n");
    ASSERT_NOT_EQUAL(notExpected, actual);
}


/**********************************************************
 * Tests init()
 *********************************************************/

void test_init_changes_state_to_STATE_LANDING_if_battery_0() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 0;
    State expected = STATE_LANDING;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_LANDING_if_battery_0\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_changes_state_to_STATE_LANDING_if_battery_14() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 14;
    State expected = STATE_LANDING;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_LANDING_if_battery_14\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_changes_state_to_STATE_LANDING_if_battery_15() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 15;
    State expected = STATE_LANDING;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_LANDING_if_battery_15\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_16() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 16;
    State expected = STATE_RETURN_TO_BASE;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_16\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_20() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 20;
    State expected = STATE_RETURN_TO_BASE;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_20\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_30() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 30;
    State expected = STATE_RETURN_TO_BASE;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_30\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_31() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 31;
    State expected = STATE_CRASHED;
    controlUnit->state = expected;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_31\n");
    ASSERT_EQUAL(expected, actual);
}

void test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_100() {
    setupControlUnit();
    // Fixture
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 100;
    State expected = STATE_CRASHED;
    controlUnit->state = expected;

    // Test
    controlUnit->init(coords, rangingDeck, battery);
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_100\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests dispatchState()
 *********************************************************/

void test_dispatchState_state_doesnt_change_if_STATE_STANDBY() {
    setupControlUnit();
    // Fixture
    State expected = STATE_STANBY;
    controlUnit->state = expected;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_STANDBY\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_0() {
    setupControlUnit();
    // Fixture
    State expected = STATE_TAKEOFF;
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 0;
    controlUnit->init(coords, rangingDeck, battery);
    controlUnit->state = STATE_TAKEOFF;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_0\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_29() {
    setupControlUnit();
    // Fixture
    State expected = STATE_TAKEOFF;
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 29;
    controlUnit->init(coords, rangingDeck, battery);
    controlUnit->state = STATE_TAKEOFF;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_29\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_IN_MISSION() {
    setupControlUnit();
    // Fixture
    State expected = STATE_IN_MISSION;
    controlUnit->state = STATE_IN_MISSION;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_IN_MISSION\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_change_if_STATE_LANDING_and_nearDestination() {
    setupControlUnit();
    // Fixture
    State expected = STATE_STANBY;
    controlUnit->state = STATE_LANDING;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_change_if_STATE_LANDING_and_nearDestination\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_NEED_RECHARGE() {
    setupControlUnit();
    // Fixture
    State expected = STATE_NEED_RECHARGE;
    controlUnit->state = STATE_NEED_RECHARGE;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_NEED_RECHARGE\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_CHARGING() {
    setupControlUnit();
    // Fixture
    State expected = STATE_CHARGING;
    controlUnit->state = STATE_CHARGING;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_CHARGING\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_UPDATING() {
    setupControlUnit();
    // Fixture
    State expected = STATE_UPDATING;
    controlUnit->state = STATE_UPDATING;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_UPDATING\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_CRASHED() {
    setupControlUnit();
    // Fixture
    State expected = STATE_CRASHED;
    controlUnit->state = STATE_CRASHED;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_CRASHED\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_STATE_RETURN_TO_BASE_and_not_returnToBase() {
    setupControlUnit();
    // Fixture
    State expected = STATE_RETURN_TO_BASE;
    controlUnit->state = STATE_RETURN_TO_BASE;
    controlUnit->mission->position.x = -10000;
    controlUnit->mission->position.y = -10000;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_STATE_RETURN_TO_BASE_and_not_returnToBase\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_changes_if_STATE_RETURN_TO_BASE_and_returnToBase() {
    setupControlUnit();
    // Fixture
    State expected = STATE_LANDING;
    controlUnit->state = STATE_RETURN_TO_BASE;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_changes_if_STATE_RETURN_TO_BASE_and_returnToBase\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_state_doesnt_change_if_invalid_state() {
    setupControlUnit();
    // Fixture
    uint8_t expected = 69;
    controlUnit->state = (State)expected;

    // Test
    controlUnit->dispatchState();
    uint8_t actual = controlUnit->state;

    // Assert
    printf("test_dispatchState_state_doesnt_change_if_invalid_state\n");
    ASSERT_EQUAL((State)expected, actual);
}

void test_dispatchState_land_if_STATE_TAKEOFF_and_newState_is_true() {
    setupControlUnit();
    // Fixture
    int16_t expected = TAKEOFF_HEIGHT;
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float rangingDeck[MAX_SENSORS];
    uint8_t battery = 100;
    controlUnit->init(coords, rangingDeck, battery);
    controlUnit->state = STATE_TAKEOFF;
    controlUnit->mission->destination.z = 69;
    controlUnit->dispatchState(); // to set private attribute newState to true
    controlUnit->state = STATE_TAKEOFF;

    // Test
    controlUnit->dispatchState();
    int16_t actual = controlUnit->mission->destination.z;

    // Assert
    printf("test_dispatchState_land_if_STATE_TAKEOFF_and_newState_is_true\n");
    ASSERT_EQUAL(expected, actual);
}

void test_dispatchState_land_if_STATE_LANDING_and_newState_is_true() {
    setupControlUnit();
    // Fixture
    int16_t expected = GROUND;
    controlUnit->state = STATE_LANDING;
    controlUnit->mission->destination.z = 69;
    controlUnit->dispatchState(); // to set private attribute newState to true
    controlUnit->state = STATE_LANDING;

    // Test
    controlUnit->dispatchState();
    int16_t actual = controlUnit->mission->destination.z;

    // Assert
    printf("test_dispatchState_land_if_STATE_LANDING_and_newState_is_true\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests takeOff()
 *********************************************************/

void test_takeOff_mission_destination_z_changes_to_TAKEOFF_HEIGHT() {
    setupControlUnit();
    // Fixture
    int16_t expected = TAKEOFF_HEIGHT;

    // Test
    controlUnit->takeOff();
    int16_t actual = controlUnit->mission->destination.z;

    // Assert
    printf("test_takeOff_mission_destination_z_changes_to_TAKEOFF_HEIGHT\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests land()
 *********************************************************/

void test_land_mission_destination_z_changes_to_GROUND() {
    setupControlUnit();
    // Fixture
    int16_t expected = GROUND;

    // Test
    controlUnit->land();
    int16_t actual = controlUnit->mission->destination.z;

    // Assert
    printf("test_land_mission_destination_z_changes_to_GROUND\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests land()
 *********************************************************/

void test_nearDestination_returns_true_if_mission_destination_0() {
    setupControlUnit();
    // Fixture
    bool expected = true;
    controlUnit->mission->destination.x = 0;
    controlUnit->mission->destination.y = 0;
    controlUnit->mission->destination.z = 0;

    // Test
    bool actual = controlUnit->nearDestination();

    // Assert
    printf("test_nearDestination_returns_true_if_mission_destination_0\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * All tests for controlUnit
 *********************************************************/

void all_tests_controlUnit() {
    test_ControlUnit_creates_new_ControlUnit();
    test_init_changes_state_to_STATE_LANDING_if_battery_0();
    test_init_changes_state_to_STATE_LANDING_if_battery_14();
    test_init_changes_state_to_STATE_LANDING_if_battery_15();
    test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_16();
    test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_20();
    test_init_changes_state_to_STATE_RETURN_TO_BASE_if_battery_30();
    test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_31();
    test_init_doesnt_change_state_to_STATE_RETURN_TO_BASE_if_battery_100();
    test_dispatchState_state_doesnt_change_if_STATE_STANDBY();
    test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_0();
    test_dispatchState_state_doesnt_change_if_STATE_TAKEOFF_and_battery_29();
    test_dispatchState_state_doesnt_change_if_STATE_IN_MISSION();
    test_dispatchState_state_change_if_STATE_LANDING_and_nearDestination();
    test_dispatchState_state_doesnt_change_if_STATE_NEED_RECHARGE();
    test_dispatchState_state_doesnt_change_if_STATE_CHARGING();
    test_dispatchState_state_doesnt_change_if_STATE_UPDATING();
    test_dispatchState_state_doesnt_change_if_STATE_CRASHED();
    test_dispatchState_state_doesnt_change_if_STATE_RETURN_TO_BASE_and_not_returnToBase();
    test_dispatchState_state_changes_if_STATE_RETURN_TO_BASE_and_returnToBase();
    test_dispatchState_state_doesnt_change_if_invalid_state();
    test_dispatchState_land_if_STATE_TAKEOFF_and_newState_is_true();
    test_dispatchState_land_if_STATE_LANDING_and_newState_is_true();
    test_takeOff_mission_destination_z_changes_to_TAKEOFF_HEIGHT();
    test_land_mission_destination_z_changes_to_GROUND();
    test_nearDestination_returns_true_if_mission_destination_0();
}


#endif

