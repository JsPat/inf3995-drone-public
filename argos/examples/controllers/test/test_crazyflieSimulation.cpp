/** File under test controlUnit.cpp
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TESTS_CRAZYFLIE_SIMULATION_ARGOS
#define TESTS_CRAZYFLIE_SIMULATION_ARGOS

#include <cassert> 
#include "stdio.h"

#include "assertsTestsArgos.h"
// #include "/root/examples/controllers/crazyflie_simulation/crazyflieSimulation.h"
// #include "/root/examples/controllers/crazyflie_simulation/crazyflieSimulation.cpp"
#include "/root/examples/controllers/common/state.h"
#include "/root/examples/controllers/common/coordinates.h"


// CCrazyflieSensing* crazyflie;

/**
 * Setup function to execute before each tests
 */
void setupCCrazyflieSensing() {
    // crazyflie = new CCrazyflieSensing();
}



/**********************************************************
 * Tests CCrazyflieSensing()
 *********************************************************/

void test_crazyflie_cant_test_because_no_control_over_function_params_and_private_attributes() {
    setupCCrazyflieSensing();
    // Fixture

    // Test

    // Assert
    printf("test_crazyflie_cant_test_because_no_control_over_function_params_and_private_attributes\n");
    ASSERT_EQUAL(true, true);
}



/**********************************************************
 * All tests for crazyflie_simulation
 *********************************************************/

void all_tests_crazyflie_simulation() {
    test_crazyflie_cant_test_because_no_control_over_function_params_and_private_attributes();
}


#endif

