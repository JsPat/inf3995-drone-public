/** File under test messageServiceArgos.c
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TESTS_ARGOS
#define TESTS_ARGOS

#include <cassert> 
#include "stdio.h"

#include "test_messageServiceArgos.cpp"
#include "test_controlUnit.cpp"
#include "test_mission.cpp"
#include "test_crazyflieSimulation.cpp"

/**********************************************************
 * Main function to execute all tests
 *********************************************************/

int main() {
    all_tests_messageServiceArgos();
    printf("------------------------------------------\n");
    all_tests_controlUnit();
    printf("------------------------------------------\n");
    all_tests_mission();
    printf("------------------------------------------\n");
    all_tests_crazyflie_simulation();
    printf("------------------------------------------\n");

    return 0;
}


#endif

