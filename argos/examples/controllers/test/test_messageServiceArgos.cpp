/** File under test messageServiceArgos.cpp
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TEST_MESSAGE_SERVICE_ARGOS
#define TEST_MESSAGE_SERVICE_ARGOS

#include <cassert> 
#include "stdio.h"

#include "assertsTestsArgos.h"
#include "/root/examples/controllers/crazyflie_simulation/communicationSystem/messageServiceArgos.h"
#include "/root/examples/controllers/crazyflie_simulation/communicationSystem/messageServiceArgos.cpp"
#include "/root/examples/controllers/common/state.h"


MessageServiceArgos* messageService;

/**
 * Setup function to execute before each tests
 */
void setupMessageService() {
    messageService = new MessageServiceArgos();
    State state = STATE_STANBY;
    messageService->state = &state;
}



/**********************************************************
 * Tests MessageServiceArgos()
 *********************************************************/

void test_MessageServiceArgos_initialize_attributes() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = 69;
    // Test
    messageService = new MessageServiceArgos();
    uint16_t actual  = messageService->currentMessageId;

    // Assert
    printf("test_MessageServiceArgos_initialize_attributes\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests initSocket()
 *********************************************************/

void test_initSocket_cant_test_because_cant_simulate_socket_error_and_private_attribute() {
    setupMessageService();
    // Fixture

    // Test
    messageService->initSocket();

    // Assert
    printf("test_initSocket_cant_test_because_cant_simulate_socket_error_and_private_attribute\n");
    ASSERT_EQUAL(true, true);
}


/**********************************************************
 * Tests sendMessageArgos()
 *********************************************************/

void test_sendMessageArgos_doesnt_generate_message_if_called_1_time() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_1_time\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_2_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 2; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_2_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_3_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 3; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_3_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_4_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 4; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_4_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_5_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 5; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_5_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_6_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 6; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_6_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_7_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 7; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_7_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_8_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 8; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_8_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_doesnt_generate_message_if_called_9_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 9; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_doesnt_generate_message_if_called_9_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_generates_message_if_called_10_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    expected++;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_generates_message_if_called_10_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_generates_1_message_if_called_11_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    expected++;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 11; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_generates_1_message_if_called_11_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_generates_2_messages_if_called_20_times() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    expected += 2;
    uint8_t batteryLevel;
    float rangingDeck[4];
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    for (int i = 0; i < 20; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    uint16_t actual = messageService->currentMessageId;

    // Assert
    printf("test_sendMessageArgos_generates_2_messages_if_called_20_times\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_batteryLevel() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.batteryLevel;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_batteryLevel\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck0() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.distanceFront;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck0\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck1() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.distanceLeft;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck1\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck2() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.distanceBack;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck2\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck3() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.distanceRight;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck3\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_x() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.dronePositionX;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_y() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.dronePositionY;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_z() {
    setupMessageService();
    // Fixture
    int16_t expected = 69;
    messageService->currentMessageId = expected;
    uint8_t batteryLevel = expected;
    float rangingDeck[4] = {(float)expected, (float)expected, (float)expected, (float)expected};
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    Coordinates coordsPrevious = {
        .x = expected,
        .y = expected,
        .z = expected,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.dronePositionZ;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_z\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_x() {
    setupMessageService();
    // Fixture
    int16_t value = 69;
    int16_t expected = messageService->getSpeed(value, value);
    messageService->currentMessageId = value;
    uint8_t batteryLevel = value;
    float rangingDeck[4] = {(float)value, (float)value, (float)value, (float)value};
    Coordinates coords = {
        .x = value,
        .y = value,
        .z = value,
    };
    Coordinates coordsPrevious = {
        .x = value,
        .y = value,
        .z = value,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.droneSpeedX;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_y() {
    setupMessageService();
    // Fixture
    int16_t value = 69;
    int16_t expected = messageService->getSpeed(value, value);
    messageService->currentMessageId = value;
    uint8_t batteryLevel = value;
    float rangingDeck[4] = {(float)value, (float)value, (float)value, (float)value};
    Coordinates coords = {
        .x = value,
        .y = value,
        .z = value,
    };
    Coordinates coordsPrevious = {
        .x = value,
        .y = value,
        .z = value,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.droneSpeedY;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_z() {
    setupMessageService();
    // Fixture
    int16_t value = 69;
    int16_t expected = messageService->getSpeed(value, value);
    messageService->currentMessageId = value;
    uint8_t batteryLevel = value;
    float rangingDeck[4] = {(float)value, (float)value, (float)value, (float)value};
    Coordinates coords = {
        .x = value,
        .y = value,
        .z = value,
    };
    Coordinates coordsPrevious = {
        .x = value,
        .y = value,
        .z = value,
    };

    // Test
    for (int i = 0; i < 10; ++i) {
        messageService->sendMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    }
    int16_t actual = messageService->currentMessageToSend.body.droneSpeedZ;

    // Assert
    printf("test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_z\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests generateMessageArgos()
 *********************************************************/

void test_generateMessageArgos_increments_currentMessageId() {
    setupMessageService();
    // Fixture
    uint16_t expected = 0;
    messageService->currentMessageId = expected;
    expected++;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;

    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);

    // Assert
    printf("test_generateMessageArgos_increments_currentMessageId\n");
    ASSERT_EQUAL(expected, messageService->currentMessageId);
}

void test_generateMessageArgos_modifies_currentMessageToSend_sourceId() {
    setupMessageService();
    // Fixture
    uint8_t expected = 42;
    messageService->currentMessageToSend.header.sourceId = 69;
    messageService->droneSourceId = expected;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.header.sourceId;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_sourceId\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_destinationId() {
    setupMessageService();
    // Fixture
    uint8_t expected = ID_BASE_STATION;
    messageService->currentMessageToSend.header.destinationId = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.header.destinationId;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_destinationId\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_messageType() {
    setupMessageService();
    // Fixture
    uint8_t expected = INFO_MAPPING;
    messageService->currentMessageToSend.header.messageType = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.header.messageType;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_messageType\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_batteryLevel() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.batteryLevel = 69;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(expected, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.batteryLevel;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_batteryLevel\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_distanceFront() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.distanceFront = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {(float)expected, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.distanceFront;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_distanceFront\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_distanceLeft() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.distanceLeft = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, (float)expected, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.distanceLeft;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_distanceLeft\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_distanceBack() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.distanceBack = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, (float)expected, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.distanceBack;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_distanceBack\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_distanceRight() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.distanceRight = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, (float)expected};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.distanceRight;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_distanceRight\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_dronePositionX() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.dronePositionX = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = expected,
        .y = 0,
        .z = 0,
    };
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.dronePositionX;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_dronePositionX\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_dronePositionY() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.dronePositionY = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = 0,
        .y = expected,
        .z = 0,
    };
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.dronePositionY;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_dronePositionY\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_dronePositionZ() {
    setupMessageService();
    // Fixture
    uint8_t expected = 50;
    messageService->currentMessageToSend.body.dronePositionZ = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = expected,
    };
    Coordinates coordsPrevious;
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.dronePositionZ;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_dronePositionZ\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedX() {
    setupMessageService();
    // Fixture
    messageService->currentMessageToSend.body.droneSpeedX = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = 1,
        .y = 2,
        .z = 3,
    };
    Coordinates coordsPrevious = {
        .x = 4,
        .y = 5,
        .z = 6,
    };
    uint8_t expected = messageService->getSpeed(coords.x, coordsPrevious.x);
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.droneSpeedX;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedX\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedY() {
    setupMessageService();
    // Fixture
    messageService->currentMessageToSend.body.droneSpeedY = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = 1,
        .y = 2,
        .z = 3,
    };
    Coordinates coordsPrevious = {
        .x = 4,
        .y = 5,
        .z = 6,
    };
    uint8_t expected = messageService->getSpeed(coords.y, coordsPrevious.y);
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.droneSpeedY;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedY\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedZ() {
    setupMessageService();
    // Fixture
    messageService->currentMessageToSend.body.droneSpeedZ = 69;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords = {
        .x = 1,
        .y = 2,
        .z = 3,
    };
    Coordinates coordsPrevious = {
        .x = 4,
        .y = 5,
        .z = 6,
    };
    uint8_t expected = messageService->getSpeed(coords.z, coordsPrevious.z);
    
    // Test
    messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious);
    uint8_t actual = messageService->currentMessageToSend.body.droneSpeedZ;

    // Assert
    printf("test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedZ\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateMessageArgos_returns_bufferMessageToSend() {
    setupMessageService();
    // Fixture
    uint8_t expected = 69;
    messageService->droneSourceId = expected;
    messageService->bufferMessageToSend[2] = 0;
    uint8_t batteryLevel = 0;
    float rangingDeck[4] = {0.0, 0.0, 0.0, 0.0};
    Coordinates coords;
    Coordinates coordsPrevious;
    
    // Test
    uint8_t actual = (messageService->generateMessageArgos(batteryLevel, rangingDeck, coords, coordsPrevious))[2];

    // Assert
    printf("test_generateMessageArgos_returns_bufferMessageToSend\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests generateBufferFromMessage()
 *********************************************************/

void test_generateBufferFromMessage_modifies_bufferMessageToSend() {
    setupMessageService();
    // Fixture
    uint8_t expected = STATE_IN_MISSION;
    messageService->currentMessageToSend.body.droneState = expected;
    
    // Test
    uint8_t actual = (messageService->generateBufferFromMessage())[5];

    // Assert
    printf("test_generateBufferFromMessage_modifies_bufferMessageToSend\n");
    ASSERT_EQUAL(expected, actual);
}

void test_generateBufferFromMessage_returns_bufferMessageToSend() {
    setupMessageService();
    // Fixture
    uint8_t expected = 69;
    messageService->currentMessageToSend.header.sourceId = expected;
    messageService->bufferMessageToSend[2] = 0;
    
    // Test
    uint8_t actual = (messageService->generateBufferFromMessage())[2];

    // Assert
    printf("test_generateBufferFromMessage_returns_bufferMessageToSend\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests receiveMessageArgos()
 *********************************************************/

void test_receiveMessageArgos_cant_test_because_while_true_loop() {
    setupMessageService();
    // Fixture
    
    // Test

    // Assert
    printf("test_receiveMessageArgos_cant_test_because_while_true_loop\n");
    ASSERT_EQUAL(true, true);
}



/**********************************************************
 * Tests readCommandFromMessage()
 *********************************************************/

void test_readCommandFromMessage_modifies_currentMessageReceived() {
    setupMessageService();
    // Fixture
    uint8_t expected = 69;
    messageService->bufferMessageReceived[2] = expected;
    messageService->currentMessageReceived.header.sourceId = 0;
    
    // Test
    messageService->readCommandFromMessage();
    uint8_t actual = messageService->currentMessageReceived.header.sourceId;

    // Assert
    printf("test_readCommandFromMessage_modifies_currentMessageReceived\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_invalid_destinationId() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[3] = 69;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_invalid_destinationId\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_INFO_MAPPING_message() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[4] = INFO_MAPPING;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_INFO_MAPPING_message\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_big_command() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[5] = 69;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_big_command\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_small_command() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[5] = -69;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_small_command\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_NUMBER_OF_COMMANDS_command() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[5] = NUMBER_OF_COMMANDS;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_NUMBER_OF_COMMANDS_command\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_COMMAND_NO_COMMAND_command() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_NO_COMMAND;
    messageService->bufferMessageReceived[5] = NUMBER_OF_COMMANDS;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_COMMAND_NO_COMMAND_command\n");
    ASSERT_EQUAL(expected, actual);
}

void test_readCommandFromMessage_returns_command_if_COMMAND_message_with_valid_command() {
    setupMessageService();
    // Fixture
    uint8_t expected = COMMAND_LED;
    messageService->droneSourceId = 1;
    messageService->bufferMessageReceived[3] = messageService->droneSourceId;
    messageService->bufferMessageReceived[4] = COMMAND;
    messageService->bufferMessageReceived[5] = expected;
    
    // Test
    uint8_t actual = messageService->readCommandFromMessage();

    // Assert
    printf("test_readCommandFromMessage_returns_command_if_COMMAND_message_with_valid_command\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests getSpeed()
 *********************************************************/

void test_getSpeed_returns_0_if_coordinatePrevious_0() {
    setupMessageService();
    // Fixture
    int16_t expected = 0;
    
    // Test
    int16_t actual = messageService->getSpeed(expected, expected);

    // Assert
    printf("test_getSpeed_returns_0_if_coordinatePrevious_0\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_negative_coordinate_and_negative_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = -10;
    int16_t coordinate = -2;
    int16_t coordinatePrevious = -1;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_negative_coordinate_and_negative_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_negative_coordinate_and_positive_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = -30;
    int16_t coordinate = -2;
    int16_t coordinatePrevious = 1;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_negative_coordinate_and_positive_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_positive_coordinate_and_positive_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 10;
    int16_t coordinate = 2;
    int16_t coordinatePrevious = 1;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_positive_coordinate_and_positive_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_positive_coordinate_and_negative_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 30;
    int16_t coordinate = 2;
    int16_t coordinatePrevious = -1;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_positive_coordinate_and_negative_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_big_coordinate_and_big_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 20000;
    int16_t coordinate = 1000;
    int16_t coordinatePrevious = -1000;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_big_coordinate_and_big_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_big_coordinate_and_small_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 20050;
    int16_t coordinate = 2000;
    int16_t coordinatePrevious = -5;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_big_coordinate_and_small_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_small_coordinate_and_small_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 100;
    int16_t coordinate = 5;
    int16_t coordinatePrevious = -5;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_small_coordinate_and_small_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}

void test_getSpeed_works_with_small_coordinate_and_big_coordinatePrevious() {
    setupMessageService();
    // Fixture
    int16_t expected = 20050;
    int16_t coordinate = 5;
    int16_t coordinatePrevious = -2000;
    
    // Test
    int16_t actual = messageService->getSpeed(coordinate, coordinatePrevious);

    // Assert
    printf("test_getSpeed_works_with_small_coordinate_and_big_coordinatePrevious\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * All tests for messageServiceArgos
 *********************************************************/

void all_tests_messageServiceArgos() {
    test_MessageServiceArgos_initialize_attributes();
    test_sendMessageArgos_doesnt_generate_message_if_called_1_time();
    test_sendMessageArgos_doesnt_generate_message_if_called_2_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_3_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_4_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_5_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_6_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_7_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_8_times();
    test_sendMessageArgos_doesnt_generate_message_if_called_9_times();
    test_sendMessageArgos_generates_message_if_called_10_times();
    test_sendMessageArgos_generates_1_message_if_called_11_times();
    test_sendMessageArgos_generates_2_messages_if_called_20_times();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_batteryLevel();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck0();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck1();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck2();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_rangingDeck3();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_x();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_y();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coords_z();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_x();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_y();
    test_sendMessageArgos_calls_generateMessageArgos_with_received_parameters_coordsPrevious_z();
    test_initSocket_cant_test_because_cant_simulate_socket_error_and_private_attribute();
    test_generateMessageArgos_increments_currentMessageId();
    test_generateMessageArgos_modifies_currentMessageToSend_sourceId();
    test_generateMessageArgos_modifies_currentMessageToSend_destinationId();
    test_generateMessageArgos_modifies_currentMessageToSend_messageType();
    test_generateMessageArgos_modifies_currentMessageToSend_batteryLevel();
    test_generateMessageArgos_modifies_currentMessageToSend_distanceFront();
    test_generateMessageArgos_modifies_currentMessageToSend_distanceLeft();
    test_generateMessageArgos_modifies_currentMessageToSend_distanceBack();
    test_generateMessageArgos_modifies_currentMessageToSend_distanceRight();
    test_generateMessageArgos_modifies_currentMessageToSend_dronePositionX();
    test_generateMessageArgos_modifies_currentMessageToSend_dronePositionY();
    test_generateMessageArgos_modifies_currentMessageToSend_dronePositionZ();
    test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedX();
    test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedY();
    test_generateMessageArgos_modifies_currentMessageToSend_droneSpeedZ();
    test_generateMessageArgos_returns_bufferMessageToSend();
    test_generateBufferFromMessage_modifies_bufferMessageToSend();
    test_generateBufferFromMessage_returns_bufferMessageToSend();
    test_receiveMessageArgos_cant_test_because_while_true_loop();
    test_readCommandFromMessage_modifies_currentMessageReceived();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_invalid_destinationId();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_INFO_MAPPING_message();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_big_command();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_too_small_command();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_NUMBER_OF_COMMANDS_command();
    test_readCommandFromMessage_returns_COMMAND_NO_COMMAND_if_COMMAND_message_with_COMMAND_NO_COMMAND_command();
    test_readCommandFromMessage_returns_command_if_COMMAND_message_with_valid_command();
    test_getSpeed_returns_0_if_coordinatePrevious_0();
    test_getSpeed_works_with_negative_coordinate_and_negative_coordinatePrevious();
    test_getSpeed_works_with_negative_coordinate_and_positive_coordinatePrevious();
    test_getSpeed_works_with_positive_coordinate_and_positive_coordinatePrevious();
    test_getSpeed_works_with_positive_coordinate_and_negative_coordinatePrevious();
    test_getSpeed_works_with_big_coordinate_and_big_coordinatePrevious();
    test_getSpeed_works_with_big_coordinate_and_small_coordinatePrevious();
    test_getSpeed_works_with_small_coordinate_and_small_coordinatePrevious();
    test_getSpeed_works_with_small_coordinate_and_big_coordinatePrevious();
}


#endif

