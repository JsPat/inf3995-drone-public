/** File under test controlUnit.cpp
 * 
 * Inspired from the Bitcraze Crazyflie tests
 * that can be found here: https://github.com/bitcraze/crazyflie-firmware/tree/master/test
*/

#ifndef TESTS_MISSION_ARGOS
#define TESTS_MISSION_ARGOS

#include <cassert> 
#include "stdio.h"

#include "assertsTestsArgos.h"
#include "/root/examples/controllers/crazyflie_simulation/controlSystem/mission.h"
#include "/root/examples/controllers/crazyflie_simulation/controlSystem/mission.cpp"
#include "/root/examples/controllers/common/state.h"


Mission* mission;

/**
 * Setup function to execute before each tests
 */
void setupMission() {
    mission = new Mission();
}



/**********************************************************
 * Tests Mission()
 *********************************************************/

void test_Mission_creates_new_Mission() {
    setupMission();
    // Fixture
    uint8_t expected = 0;
    mission->position.x = 69;
    mission->position.y = 69;
    mission->position.z = 69;

    // Test
    mission = new Mission();
    uint8_t actualX = mission->position.x;
    uint8_t actualY = mission->position.y;
    uint8_t actualZ = mission->position.z;

    // Assert
    printf("test_Mission_creates_new_Mission\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}


/**********************************************************
 * Tests init()
 *********************************************************/

void test_init_modifies_position() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    float sensors[MAX_SENSORS];
    mission->position.x = 0;
    mission->position.y = 0;
    mission->position.z = 0;

    // Test
    mission->init(coords, sensors);
    int16_t actualX = mission->position.x;
    int16_t actualY = mission->position.y;
    int16_t actualZ = mission->position.z;

    // Assert
    printf("test_init_modifies_position\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}

void test_init_modifies_destination_if_not_set() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    float sensors[MAX_SENSORS];
    mission->destination.x = 0;
    mission->destination.y = 0;
    mission->destination.z = 0;

    // Test
    mission->init(coords, sensors);
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_init_modifies_destination_if_not_set\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}

void test_init_doesnt_modify_destination_if_set_is_true() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    Coordinates coords = {
        .x = 0,
        .y = 0,
        .z = 0,
    };
    float sensors[MAX_SENSORS];
    mission->destination.x = 0;
    mission->destination.y = 0;
    mission->destination.z = 0;
    // call init to change set to true
    mission->init(coords, sensors);
    mission->destination.x = expected;
    mission->destination.y = expected;
    mission->destination.z = expected;

    // Test
    mission->init(coords, sensors);
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_init_doesnt_modify_destination_if_set_is_true\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}

void test_init_modifies_basePosition_x_y_to_base_station_positions() {
    setupMission();
    // Fixture
    int16_t expectedX = BASE_STATION_POSITION_X;
    int16_t expectedY = BASE_STATION_POSITION_Y;
    Coordinates coords = {
        .x = expectedX,
        .y = expectedY,
    };
    float sensors[MAX_SENSORS];
    mission->basePosition.x = 0;
    mission->basePosition.y = 0;

    // Test
    mission->init(coords, sensors);
    int16_t actualX = mission->basePosition.x;
    int16_t actualY = mission->basePosition.y;

    // Assert
    printf("test_init_modifies_basePosition_x_y_to_base_station_positions\n");
    ASSERT_EQUAL(expectedX, actualX);
    ASSERT_EQUAL(expectedY, actualY);
}

void test_init_modifies_basePosition_z_if_not_set() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    Coordinates coords = {
        .x = expected,
        .y = expected,
        .z = expected,
    };
    float sensors[MAX_SENSORS];
    mission->basePosition.z = 0;

    // Test
    mission->init(coords, sensors);
    int16_t actualZ = mission->basePosition.z;

    // Assert
    printf("test_init_modifies_basePosition_z_if_not_set\n");
    ASSERT_EQUAL(expected, actualZ);
}


/**********************************************************
 * Tests executeMission()
 *********************************************************/

void test_executeMission_cant_test_because_of_private_attributes_and_class_functions() {
    setupMission();
    // Fixture

    // Test
    mission->executeMission();

    // Assert
    printf("test_executeMission_cant_test_because_of_private_attributes_and_class_functions\n");
    ASSERT_EQUAL(true, true);
}


/**********************************************************
 * Tests move()
 *********************************************************/

void test_move_modifies_destination_coordinates() {
    setupMission();
    // Fixture
    float expected = 69.0;
    int16_t before = 0;
    mission->destination.x = before;
    mission->destination.y = before;
    mission->destination.z = before;

    // Test
    mission->move(expected, expected, expected);
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_move_modifies_destination_coordinates\n");
    ASSERT_EQUAL(expected + before, actualX);
    ASSERT_EQUAL(expected + before, actualY);
    ASSERT_EQUAL(expected + before, actualZ);
}

/**********************************************************
 * Tests moveLeft()
 *********************************************************/

void test_moveLeft_doesnt_modifiy_destination_x() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.x = expected;

    // Test
    mission->moveLeft();
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveLeft_doesnt_modifiy_destination_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_moveLeft_modifies_destination_y_to_positive_speed() {
    setupMission();
    // Fixture
    int16_t expected = NORMAL_SPEED;
    int16_t before = 0;
    mission->destination.y = before;

    // Test
    mission->moveLeft();
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveLeft_modifies_destination_y_to_positive_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveLeft_doesnt_modifiy_destination_z() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.z = expected;

    // Test
    mission->moveLeft();
    int16_t actual = mission->destination.z;
    // Assert
    printf("test_moveLeft_doesnt_modifiy_destination_z\n");
    ASSERT_EQUAL(expected, actual);
}

/**********************************************************
 * Tests moveRight()
 *********************************************************/

void test_moveRight_doesnt_modifiy_destination_x() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.x = expected;

    // Test
    mission->moveRight();
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveRight_doesnt_modifiy_destination_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_moveRight_modifies_destination_y_to_negative_speed() {
    setupMission();
    // Fixture
    int16_t expected = -NORMAL_SPEED;
    int16_t before = 0;
    mission->destination.y = before;

    // Test
    mission->moveRight();
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveRight_modifies_destination_y_to_negative_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveRight_doesnt_modifiy_destination_z() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.z = expected;

    // Test
    mission->moveRight();
    int16_t actual = mission->destination.z;
    // Assert
    printf("test_moveRight_doesnt_modifiy_destination_z\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests moveFront()
 *********************************************************/

void test_moveFront_modifies_destination_x_to_positive_speed() {
    setupMission();
    // Fixture
    float expected = NORMAL_SPEED;
    int16_t before = 0;
    mission->destination.x = before;

    // Test
    mission->moveFront();
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveFront_modifies_destination_x_to_positive_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveFront_modifies_doesnt_modifiy_destination_y() {
    setupMission();
    // Fixture
    int16_t expected = 69.0;
    mission->destination.y = expected;

    // Test
    mission->moveFront();
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveFront_modifies_doesnt_modifiy_destination_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_moveFront_doesnt_modifiy_destination_z() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.z = expected;

    // Test
    mission->moveFront();
    int16_t actual = mission->destination.z;
    // Assert
    printf("test_moveFront_doesnt_modifiy_destination_z\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests moveBack()
 *********************************************************/

void test_moveBack_modifies_destination_x_to_negative_speed() {
    setupMission();
    // Fixture
    float expected = -NORMAL_SPEED;
    int16_t before = 0;
    mission->destination.x = before;

    // Test
    mission->moveBack();
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveBack_modifies_destination_x_to_negative_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveBack_modifies_doesnt_modifiy_destination_y() {
    setupMission();
    // Fixture
    int16_t expected = 69.0;
    mission->destination.y = expected;

    // Test
    mission->moveBack();
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveBack_modifies_doesnt_modifiy_destination_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_moveBack_doesnt_modifiy_destination_z() {
    setupMission();
    // Fixture
    float expected = 69.0;
    mission->destination.z = expected;

    // Test
    mission->moveBack();
    int16_t actual = mission->destination.z;
    // Assert
    printf("test_moveBack_doesnt_modifiy_destination_z\n");
    ASSERT_EQUAL(expected, actual);
}

/**********************************************************
 * Tests moveDirection()
 *********************************************************/

void test_moveDirection_goes_to_front_if_FRONTSIDE_so_destination_x_positive_speed() {
    setupMission();
    // Fixture
    int16_t expected = NORMAL_SPEED;
    int16_t before = 0;
    int16_t side = FRONTSIDE;
    mission->destination.x = before;

    // Test
    mission->moveDirection(side);
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveDirection_goes_to_front_if_FRONTSIDE_so_destination_x_positive_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveDirection_goes_to_left_if_LEFTSIDE_so_destination_y_positive_speed() {
    setupMission();
    // Fixture
    int16_t expected = NORMAL_SPEED;
    int16_t before = 0;
    int16_t side = LEFTSIDE;
    mission->destination.y = before;

    // Test
    mission->moveDirection(side);
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveDirection_goes_to_left_if_LEFTSIDE_so_destination_y_positive_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveDirection_goes_to_front_if_BACKSIDE_so_destination_x_negative_speed() {
    setupMission();
    // Fixture
    int16_t expected = -NORMAL_SPEED;
    int16_t before = 0;
    int16_t side = BACKSIDE;
    mission->destination.x = before;

    // Test
    mission->moveDirection(side);
    int16_t actual = mission->destination.x;
    // Assert
    printf("test_moveDirection_goes_to_front_if_BACKSIDE_so_destination_x_negative_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveDirection_goes_to_left_if_RIGHTSIDE_so_destination_y_negative_speed() {
    setupMission();
    // Fixture
    int16_t expected = -NORMAL_SPEED;
    int16_t before = 0;
    int16_t side = RIGHTSIDE;
    mission->destination.y = before;

    // Test
    mission->moveDirection(side);
    int16_t actual = mission->destination.y;
    // Assert
    printf("test_moveDirection_goes_to_left_if_RIGHTSIDE_so_destination_y_negative_speed\n");
    ASSERT_EQUAL(expected + before, actual);
}

void test_moveDirection_stop_if_STOP() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    int16_t side = STOP;
    mission->position.x = expected;
    mission->position.y = expected;
    mission->position.z = expected;
    mission->destination.x = 0;
    mission->destination.y = 0;
    mission->destination.z = 0;

    // Test
    mission->moveDirection(side);
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_moveDirection_stop_if_STOP\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}


/**********************************************************
 * Tests stop()
 *********************************************************/

void test_stop_drone_movement() {
    setupMission();
    // Fixture
    int16_t expected = 0;
    int16_t side = STOP;
    mission->position.x = expected;
    mission->position.y = expected;
    mission->position.z = expected;
    mission->destination.x = 69;
    mission->destination.y = 69;
    mission->destination.z = 69;

    // Test
    mission->stop();
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_stop_drone_movement\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}



/**********************************************************
 * Tests wallDetection()
 *********************************************************/

void test_wallDetection_stop_if_wall_close() {
    setupMission();
    // Fixture
    int16_t expected = 69;
    int16_t side = FRONTSIDE;
    mission->destination.x = 420;
    mission->destination.y = 420;
    mission->destination.z = 420;
    mission->position.x = expected;
    mission->position.y = expected;
    mission->position.z = expected;

    // Test
    mission->wallDetection(side);
    int16_t actualX = mission->destination.x;
    int16_t actualY = mission->destination.y;
    int16_t actualZ = mission->destination.z;

    // Assert
    printf("test_wallDetection_stop_if_wall_close\n");
    ASSERT_EQUAL(expected, actualX);
    ASSERT_EQUAL(expected, actualY);
    ASSERT_EQUAL(expected, actualZ);
}

void test_wallDetection_cant_test_other_if_else_for_because_of_private_attributes() {
    setupMission();
    // Fixture
    uint8_t side = RIGHTSIDE;

    // Test
    mission->wallDetection(side);

    // Assert
    printf("test_wallDetection_cant_test_other_if_else_for_because_of_private_attributes\n");
    ASSERT_EQUAL(true, true);
}



/**********************************************************
 * Tests isWallClose()
 *********************************************************/

void test_isWallClose_return_true_if_distance_smaller_than_lenght_and_positive_distance() {
    setupMission();
    // Fixture
    bool expected = true;
    int16_t side = FRONTSIDE;
    float lenght = 100.0;

    // Test
    bool actual = mission->isWallClose(side, lenght);

    // Assert
    printf("test_isWallClose_return_true_if_distance_FRONTSIDE_smaller_than_lenght_and_positive_distance\n");
    ASSERT_EQUAL(expected, actual);
}

void test_isWallClose_return_false_if_distance_bigger_than_lenght() {
    setupMission();
    // Fixture
    bool expected = false;
    int16_t side = FRONTSIDE;
    float lenght = -100.0;

    // Test
    bool actual = mission->isWallClose(side, lenght);

    // Assert
    printf("test_isWallClose_return_false_if_distance_bigger_than_lenght\n");
    ASSERT_EQUAL(expected, actual);
}


/**********************************************************
 * Tests returnToBase()
 *********************************************************/

void test_returnToBase_returns_true_if_at_same_position_base_x_y() {
    setupMission();
    // Fixture
    bool expected = true;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X;
    mission->position.y = BASE_STATION_POSITION_Y;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_true_if_at_same_position_base_x_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_true_if_near_position_base_x_y() {
    setupMission();
    // Fixture
    bool expected = true;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X + 500;
    mission->position.y = BASE_STATION_POSITION_Y + 500;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_true_if_near_position_base_x_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_at_1m_position_base_x_y() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X + 1000;
    mission->position.y = BASE_STATION_POSITION_Y + 1000;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_true_if_near_position_base_x_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_at_1m_position_base_x() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X + 1000;
    mission->position.y = BASE_STATION_POSITION_Y;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_false_if_at_1m_position_base_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_at_1m_position_base_y() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X;
    mission->position.y = BASE_STATION_POSITION_Y + 1000;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_false_if_at_1m_position_base_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_far_away_position_base_x_y() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X + 10000;
    mission->position.y = BASE_STATION_POSITION_Y + 10000;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_false_if_far_away_position_base_x_y\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_far_away_position_base_x() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X + 10000;
    mission->position.y = BASE_STATION_POSITION_Y;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_false_if_far_away_position_base_x\n");
    ASSERT_EQUAL(expected, actual);
}

void test_returnToBase_returns_false_if_far_away_position_base_y() {
    setupMission();
    // Fixture
    bool expected = false;
    mission->basePosition.x = BASE_STATION_POSITION_X;
    mission->basePosition.y = BASE_STATION_POSITION_Y;
    mission->position.x = BASE_STATION_POSITION_X;
    mission->position.y = BASE_STATION_POSITION_Y + 10000;

    // Test
    bool actual = mission->returnToBase();

    // Assert
    printf("test_returnToBase_returns_false_if_far_away_position_base_y\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * Tests logicRTB()
 *********************************************************/

void test_logicRTB_modifies_direction_if_changeLeftSide_FRONTSIDE() {
    setupMission();
    // Fixture
    int16_t expected = NORMAL_SPEED;
    uint8_t side = FRONTSIDE;
    mission->destination.x = 0;

    // Test
    mission->logicRTB(side);
    int16_t actual = mission->destination.x;

    // Assert
    printf("test_logicRTB_modifies_direction_if_changeLeftSide_FRONTSIDE\n");
    ASSERT_EQUAL(expected, actual);
}

void test_logicRTB_modifies_direction_if_changeLeftSide_LEFTSIDE() {
    setupMission();
    // Fixture
    int16_t expected = NORMAL_SPEED;
    uint8_t side = LEFTSIDE;
    mission->destination.y = 0;

    // Test
    mission->logicRTB(side);
    int16_t actual = mission->destination.y;

    // Assert
    printf("test_logicRTB_modifies_direction_if_changeLeftSide_LEFTSIDE\n");
    ASSERT_EQUAL(expected, actual);
}

void test_logicRTB_modifies_direction_if_changeLeftSide_BACKSIDE() {
    setupMission();
    // Fixture
    int16_t expected = -NORMAL_SPEED;
    uint8_t side = BACKSIDE;
    mission->destination.x = 0;

    // Test
    mission->logicRTB(side);
    int16_t actual = mission->destination.x;

    // Assert
    printf("test_logicRTB_modifies_direction_if_changeLeftSide_BACKSIDE\n");
    ASSERT_EQUAL(expected, actual);
}

void test_logicRTB_modifies_direction_if_changeLeftSide_RIGHTSIDE() {
    setupMission();
    // Fixture
    int16_t expected = -NORMAL_SPEED;
    uint8_t side = RIGHTSIDE;
    mission->destination.y = 0;

    // Test
    mission->logicRTB(side);
    int16_t actual = mission->destination.y;

    // Assert
    printf("test_logicRTB_modifies_direction_if_changeLeftSide_RIGHTSIDE\n");
    ASSERT_EQUAL(expected, actual);
}

void test_logicRTB_cant_test_other_else_if_because_only_private_attributes_modified() {
    setupMission();
    // Fixture
    uint8_t side = RIGHTSIDE;

    // Test
    mission->logicRTB(side);

    // Assert
    printf("test_logicRTB_cant_test_other_else_if_because_only_private_attributes_modified\n");
    ASSERT_EQUAL(true, true);
}



/**********************************************************
 * Tests changeSide()
 *********************************************************/

void test_changeSide_doesnt_modify_side_if_already_desired_side() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 0;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_doesnt_modify_side_if_already_desired_side\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_FRONTSIDE_to_LEFTSIDE_if_one_change() {
    setupMission();
    // Fixture
    uint8_t expected = LEFTSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 1;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_FRONTSIDE_to_LEFTSIDE_if_one_change\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_LEFTSIDE_to_BACKSIDE_if_one_change() {
    setupMission();
    // Fixture
    uint8_t expected = BACKSIDE;
    uint8_t side = LEFTSIDE;
    uint8_t nbChange = 1;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_LEFTSIDE_to_BACKSIDE_if_one_change\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_BACKSIDE_to_RIGHTSIDE_if_one_change() {
    setupMission();
    // Fixture
    uint8_t expected = RIGHTSIDE;
    uint8_t side = BACKSIDE;
    uint8_t nbChange = 1;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_BACKSIDE_to_RIGHTSIDE_if_one_change\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_RIGHTSIDE_to_FRONTSIDE_if_one_change() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = RIGHTSIDE;
    uint8_t nbChange = 1;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_RIGHTSIDE_to_FRONTSIDE_if_one_change\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_FRONTSIDE_to_BACKSIDE_if_two_changes() {
    setupMission();
    // Fixture
    uint8_t expected = BACKSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 2;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_FRONTSIDE_to_BACKSIDE_if_two_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_LEFTSIDE_to_RIGHTSIDE_if_two_changes() {
    setupMission();
    // Fixture
    uint8_t expected = RIGHTSIDE;
    uint8_t side = LEFTSIDE;
    uint8_t nbChange = 2;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_LEFTSIDE_to_RIGHTSIDE_if_two_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_BACKSIDE_to_FRONTSIDE_if_two_changes() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = BACKSIDE;
    uint8_t nbChange = 2;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_BACKSIDE_to_FRONTSIDE_if_two_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_RIGHTSIDE_to_LEFTSIDE_if_two_changes() {
    setupMission();
    // Fixture
    uint8_t expected = LEFTSIDE;
    uint8_t side = RIGHTSIDE;
    uint8_t nbChange = 2;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_RIGHTSIDE_to_LEFTSIDE_if_two_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_FRONTSIDE_to_RIGHTSIDE_if_3_changes() {
    setupMission();
    // Fixture
    uint8_t expected = RIGHTSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 3;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_FRONTSIDE_to_RIGHTSIDE_if_3_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_LEFTSIDE_to_FRONTSIDE_if_3_changes() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = LEFTSIDE;
    uint8_t nbChange = 3;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_LEFTSIDE_to_FRONTSIDE_if_3_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_BACKSIDE_to_LEFTSIDE_if_3_changes() {
    setupMission();
    // Fixture
    uint8_t expected = LEFTSIDE;
    uint8_t side = BACKSIDE;
    uint8_t nbChange = 3;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_BACKSIDE_to_LEFTSIDE_if_3_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_RIGHTSIDE_to_BACKSIDE_if_3_changes() {
    setupMission();
    // Fixture
    uint8_t expected = BACKSIDE;
    uint8_t side = RIGHTSIDE;
    uint8_t nbChange = 3;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_RIGHTSIDE_to_BACKSIDE_if_3_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_FRONTSIDE_to_FRONTSIDE_if_4_changes() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 4;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_FRONTSIDE_to_FRONTSIDE_if_4_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_LEFTSIDE_to_LEFTSIDE_if_4_changes() {
    setupMission();
    // Fixture
    uint8_t expected = LEFTSIDE;
    uint8_t side = LEFTSIDE;
    uint8_t nbChange = 4;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_LEFTSIDE_to_LEFTSIDE_if_4_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_BACKSIDE_to_BACKSIDE_if_4_changes() {
    setupMission();
    // Fixture
    uint8_t expected = BACKSIDE;
    uint8_t side = BACKSIDE;
    uint8_t nbChange = 4;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_BACKSIDE_to_BACKSIDE_if_4_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_modifies_RIGHTSIDE_to_RIGHTSIDE_if_4_changes() {
    setupMission();
    // Fixture
    uint8_t expected = RIGHTSIDE;
    uint8_t side = RIGHTSIDE;
    uint8_t nbChange = 4;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_modifies_RIGHTSIDE_to_RIGHTSIDE_if_4_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_works_with_big_number_of_changes() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = FRONTSIDE;
    uint8_t nbChange = 40;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_works_with_big_number_of_changes\n");
    ASSERT_EQUAL(expected, actual);
}

void test_changeSide_doesnt_modify_if_negative_number_of_changes() {
    setupMission();
    // Fixture
    uint8_t expected = FRONTSIDE;
    uint8_t side = FRONTSIDE;
    int8_t nbChange = -1;

    // Test
    uint8_t actual = mission->changeSide(side, nbChange);

    // Assert
    printf("test_changeSide_doesnt_modify_if_negative_number_of_changes\n");
    ASSERT_EQUAL(expected, actual);
}



/**********************************************************
 * All tests for mission
 *********************************************************/

void all_tests_mission() {
    test_Mission_creates_new_Mission();
    test_init_modifies_position();
    test_init_modifies_destination_if_not_set();
    test_init_doesnt_modify_destination_if_set_is_true();
    test_init_modifies_basePosition_x_y_to_base_station_positions();
    test_init_modifies_basePosition_z_if_not_set();
    test_executeMission_cant_test_because_of_private_attributes_and_class_functions();
    test_move_modifies_destination_coordinates();
    test_moveLeft_doesnt_modifiy_destination_x();
    test_moveLeft_modifies_destination_y_to_positive_speed();
    test_moveLeft_doesnt_modifiy_destination_z();
    test_moveRight_doesnt_modifiy_destination_x();
    test_moveRight_modifies_destination_y_to_negative_speed();
    test_moveRight_doesnt_modifiy_destination_z();
    test_moveFront_modifies_destination_x_to_positive_speed();
    test_moveFront_modifies_doesnt_modifiy_destination_y();
    test_moveFront_doesnt_modifiy_destination_z();
    test_moveBack_modifies_destination_x_to_negative_speed();
    test_moveBack_modifies_doesnt_modifiy_destination_y();
    test_moveBack_doesnt_modifiy_destination_z();
    test_moveDirection_goes_to_front_if_FRONTSIDE_so_destination_x_positive_speed();
    test_moveDirection_goes_to_left_if_LEFTSIDE_so_destination_y_positive_speed();
    test_moveDirection_goes_to_front_if_BACKSIDE_so_destination_x_negative_speed();
    test_moveDirection_goes_to_left_if_RIGHTSIDE_so_destination_y_negative_speed();
    test_moveDirection_stop_if_STOP();
    test_stop_drone_movement();
    test_wallDetection_stop_if_wall_close();
    test_wallDetection_cant_test_other_if_else_for_because_of_private_attributes();
    test_isWallClose_return_true_if_distance_smaller_than_lenght_and_positive_distance();
    test_isWallClose_return_false_if_distance_bigger_than_lenght();
    test_returnToBase_returns_true_if_at_same_position_base_x_y();
    test_returnToBase_returns_true_if_near_position_base_x_y();
    test_returnToBase_returns_false_if_far_away_position_base_x_y();
    test_returnToBase_returns_false_if_far_away_position_base_x();
    test_returnToBase_returns_false_if_far_away_position_base_y();
    test_logicRTB_modifies_direction_if_changeLeftSide_FRONTSIDE();
    test_logicRTB_modifies_direction_if_changeLeftSide_LEFTSIDE();
    test_logicRTB_modifies_direction_if_changeLeftSide_BACKSIDE();
    test_logicRTB_modifies_direction_if_changeLeftSide_RIGHTSIDE();
    test_logicRTB_cant_test_other_else_if_because_only_private_attributes_modified();
    test_changeSide_doesnt_modify_side_if_already_desired_side();
    test_changeSide_modifies_FRONTSIDE_to_LEFTSIDE_if_one_change();
    test_changeSide_modifies_LEFTSIDE_to_BACKSIDE_if_one_change();
    test_changeSide_modifies_BACKSIDE_to_RIGHTSIDE_if_one_change();
    test_changeSide_modifies_RIGHTSIDE_to_FRONTSIDE_if_one_change();
    test_changeSide_modifies_FRONTSIDE_to_BACKSIDE_if_two_changes();
    test_changeSide_modifies_LEFTSIDE_to_RIGHTSIDE_if_two_changes();
    test_changeSide_modifies_BACKSIDE_to_FRONTSIDE_if_two_changes();
    test_changeSide_modifies_RIGHTSIDE_to_LEFTSIDE_if_two_changes();
    test_changeSide_modifies_FRONTSIDE_to_RIGHTSIDE_if_3_changes();
    test_changeSide_modifies_LEFTSIDE_to_FRONTSIDE_if_3_changes();
    test_changeSide_modifies_BACKSIDE_to_LEFTSIDE_if_3_changes();
    test_changeSide_modifies_RIGHTSIDE_to_BACKSIDE_if_3_changes();
    test_changeSide_modifies_FRONTSIDE_to_FRONTSIDE_if_4_changes();
    test_changeSide_modifies_LEFTSIDE_to_LEFTSIDE_if_4_changes();
    test_changeSide_modifies_BACKSIDE_to_BACKSIDE_if_4_changes();
    test_changeSide_modifies_RIGHTSIDE_to_RIGHTSIDE_if_4_changes();
    test_changeSide_works_with_big_number_of_changes();
    test_changeSide_doesnt_modify_if_negative_number_of_changes();
}


#endif

